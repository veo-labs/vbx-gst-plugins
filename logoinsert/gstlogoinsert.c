/* GStreamer
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */

/**
 * SECTION:element-logoinsert
 *
 * The logoinsert element overlays an image loaded from file onto
 * a video stream.
 *
 * Changing the positioning or overlay width and height properties at runtime
 * is supported, but it might be prudent to to protect the property setting
 * code with GST_BASE_TRANSFORM_LOCK and GST_BASE_TRANSFORM_UNLOCK, as
 * g_object_set() is not atomic for multiple properties passed in one go.
 *
 * Changing the image at runtime is currently not supported.
 *
 * Negative offsets are also not yet supported.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! logoinsert location=image.png ! autovideosink
 * ]|
 * Overlays the image in image.png onto the test video picture produced by
 * videotestsrc.
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <arm_neon.h>
#include <gst/gst.h>
#include "gstlogoinsert.h"

#include <gst/video/gstvideometa.h>

#define BUF_ALIGN  __attribute__ ((aligned (16)))

GST_DEBUG_CATEGORY_STATIC (logoinsert_debug);
#define GST_CAT_DEFAULT logoinsert_debug

static void gst_logo_insert_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_logo_insert_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_logo_insert_finalize (GObject * object);

static gboolean gst_logo_insert_start (GstBaseTransform * trans);
static gboolean gst_logo_insert_stop (GstBaseTransform * trans);
static GstFlowReturn
gst_logo_insert_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame);
static void gst_logo_insert_before_transform (GstBaseTransform * trans,
    GstBuffer * outbuf);
static gboolean gst_logo_insert_set_info (GstVideoFilter * filter,
    GstCaps * incaps, GstVideoInfo * in_info, GstCaps * outcaps,
    GstVideoInfo * out_info);
static gboolean
gst_logo_insert_set_pixbuf_from_location (GstLogoInsert * overlay,
    const gchar * location, GError ** err);
static void gst_logo_insert_set_pixbuf (GstLogoInsert * overlay,
    GdkPixbuf * pixbuf);

static GstLogoInsertComposition *gst_logo_insert_composition_new (gint x,
    gint y, gint width, gint height, GstBuffer * logo);
static void gst_logo_insert_composition_free (GstLogoInsertComposition * comp);

enum
{
  PROP_0,
  PROP_LOCATION,
  PROP_PIXBUF,
  PROP_OFFSET_X,
  PROP_OFFSET_Y
};

#define VIDEO_FORMATS "{ NV12 }"

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (VIDEO_FORMATS))
    );

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (VIDEO_FORMATS))
    );

G_DEFINE_TYPE (GstLogoInsert, gst_logo_insert, GST_TYPE_VIDEO_FILTER);

static void
gst_logo_insert_class_init (GstLogoInsertClass * klass)
{
  GstVideoFilterClass *videofilter_class = GST_VIDEO_FILTER_CLASS (klass);
  GstBaseTransformClass *basetrans_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = gst_logo_insert_set_property;
  gobject_class->get_property = gst_logo_insert_get_property;
  gobject_class->finalize = gst_logo_insert_finalize;

  basetrans_class->start = GST_DEBUG_FUNCPTR (gst_logo_insert_start);
  basetrans_class->stop = GST_DEBUG_FUNCPTR (gst_logo_insert_stop);

  basetrans_class->before_transform =
      GST_DEBUG_FUNCPTR (gst_logo_insert_before_transform);

  videofilter_class->set_info = GST_DEBUG_FUNCPTR (gst_logo_insert_set_info);
  videofilter_class->transform_frame_ip =
      GST_DEBUG_FUNCPTR (gst_logo_insert_transform_frame_ip);

  g_object_class_install_property (gobject_class, PROP_LOCATION,
      g_param_spec_string ("location", "location",
          "Location of image file to overlay", NULL, GST_PARAM_CONTROLLABLE
          | GST_PARAM_MUTABLE_PLAYING | G_PARAM_READWRITE
          | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_OFFSET_X,
      g_param_spec_int ("offset-x", "X Offset",
          "For positive value, horizontal offset of overlay image in pixels from"
          " left of video image. For negative value, horizontal offset of overlay"
          " image in pixels from right of video image", G_MININT, G_MAXINT, 0,
          GST_PARAM_CONTROLLABLE | GST_PARAM_MUTABLE_PLAYING | G_PARAM_READWRITE
          | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_OFFSET_Y,
      g_param_spec_int ("offset-y", "Y Offset",
          "For positive value, vertical offset of overlay image in pixels from"
          " top of video image. For negative value, vertical offset of overlay"
          " image in pixels from bottom of video image", G_MININT, G_MAXINT, 0,
          GST_PARAM_CONTROLLABLE | GST_PARAM_MUTABLE_PLAYING | G_PARAM_READWRITE
          | G_PARAM_STATIC_STRINGS));
  /**
   * GstLogoInsert:pixbuf:
   *
   * GdkPixbuf object to render.
   *
   * Since: 1.6
   */
  g_object_class_install_property (gobject_class, PROP_PIXBUF,
      g_param_spec_object ("pixbuf", "Pixbuf", "GdkPixbuf object to render",
          GDK_TYPE_PIXBUF, GST_PARAM_CONTROLLABLE | GST_PARAM_MUTABLE_PLAYING
          | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_template));

  gst_element_class_set_static_metadata (element_class,
      "GdkPixbuf Overlay", "Filter/Effect/Video",
      "Overlay an image onto a video stream",
      "Veo-Labs http://www.veo-labs.com/");
  GST_DEBUG_CATEGORY_INIT (logoinsert_debug, "logoinsert", 0,
      "debug category for logoinsert element");
}

static void
gst_logo_insert_init (GstLogoInsert * overlay)
{
  overlay->offset_x = 0;
  overlay->offset_y = 0;
  overlay->pixbuf = NULL;
  overlay->location = NULL;
}

void
gst_logo_insert_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (object);

  GST_OBJECT_LOCK (overlay);

  switch (property_id) {
    case PROP_LOCATION:{
      GError *err = NULL;
      g_free (overlay->location);
      overlay->location = g_value_dup_string (value);
      if (!gst_logo_insert_set_pixbuf_from_location (overlay, overlay->location,
              &err)) {
        GST_ERROR_OBJECT (overlay, "Could not load overlay image: %s",
            err->message);
        g_error_free (err);
      }
      break;
    }
    case PROP_PIXBUF:{
      GdkPixbuf *pixbuf = g_value_get_object (value);
      gst_logo_insert_set_pixbuf (overlay, g_object_ref (pixbuf));
      break;
    }
    case PROP_OFFSET_X:
      overlay->offset_x = g_value_get_int (value);
      overlay->update_composition = TRUE;
      break;
    case PROP_OFFSET_Y:
      overlay->offset_y = g_value_get_int (value);
      overlay->update_composition = TRUE;
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }

  GST_OBJECT_UNLOCK (overlay);
}

void
gst_logo_insert_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (object);

  GST_OBJECT_LOCK (overlay);

  switch (property_id) {
    case PROP_LOCATION:
      g_value_set_string (value, overlay->location);
      break;
    case PROP_PIXBUF:
      g_value_set_object (value, overlay->pixbuf);
      break;
    case PROP_OFFSET_X:
      g_value_set_int (value, overlay->offset_x);
      break;
    case PROP_OFFSET_Y:
      g_value_set_int (value, overlay->offset_y);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }

  GST_OBJECT_UNLOCK (overlay);
}

void
gst_logo_insert_finalize (GObject * object)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (object);

  g_free (overlay->location);
  overlay->location = NULL;

  if (overlay->pixbuf != NULL)
    g_object_unref (overlay->pixbuf);

  if (overlay->comp != NULL)
    gst_logo_insert_composition_free (overlay->comp);

  G_OBJECT_CLASS (gst_logo_insert_parent_class)->finalize (object);
}

static GstBuffer *
gst_logo_insert_convert_pixbuf (GdkPixbuf * src)
{
  gint y, u, v;
  guint8 r, g, b, a;
  GstMapInfo dstmapinfo;
  GstBuffer *dst;
  guint8 *srcdata, *dstdata;
  guint8 *dst_a, *dst_y, *dst_uv;
  gint width, height, rowstride;
  gint w, h;

  width = gdk_pixbuf_get_width (src);
  rowstride = gdk_pixbuf_get_rowstride (src);
  height = gdk_pixbuf_get_height (src);
  srcdata = gdk_pixbuf_get_pixels (src);

  dst = gst_buffer_new_allocate (NULL, width * height * 5 / 2, NULL);
  gst_buffer_add_video_meta (dst,
      GST_VIDEO_FRAME_FLAG_NONE, GST_VIDEO_FORMAT_A420, width, height);

  if (gst_buffer_map (dst, &dstmapinfo, GST_MAP_WRITE) == FALSE) {
    GST_ERROR_OBJECT (dst, "Failed to map dst buffer data for writing");
    gst_buffer_unref (dst);
    return NULL;
  }

  dstdata = dstmapinfo.data;

  dst_a = dstdata;
  dst_y = dst_a + width * height;
  dst_uv = dst_y + width * height;

  for (h = 0; h < height; h++) {
    for (w = 0; w < width; w++) {
      r = *srcdata++;
      g = *srcdata++;
      b = *srcdata++;
      a = *srcdata++;
      *dst_a++ = a;
      y = ((66 * r + 129 * g + 25 * b + 128) >> 8) + 16;
      *dst_y++ = y;
      if ((w % 2) == 1 && (h % 2) == 1) {
        u = ((112 * b - 38 * r - 74 * g + 128) >> 8) + 128;
        v = ((112 * r - 94 * g - 18 * b + 128) >> 8) + 128;
        *dst_uv++ = u;
        *dst_uv++ = v;
      }
    }
    srcdata += rowstride - width * 4;
  }

  gst_buffer_unmap (dst, &dstmapinfo);
  GST_DEBUG_OBJECT (dst, "Image successfully converted to ANV12");

  return dst;
}

static GstLogoInsertComposition *
gst_logo_insert_composition_new (gint x, gint y, gint width, gint height,
    GstBuffer * logo)
{
  GstLogoInsertComposition *comp = g_new (GstLogoInsertComposition, 1);
  comp->x = x;
  comp->y = y;
  comp->width = width;
  comp->height = height;
  comp->logo = logo;
  return comp;
}

static void
gst_logo_insert_composition_free (GstLogoInsertComposition * comp)
{
  if (comp->logo != NULL)
    gst_buffer_unref (comp->logo);
  g_free (comp);
}

static gint
round_down (gint x, gint y)
{
  return x & ~(y - 1);
}

static void
apply_neon (guint8 * dst_y, guint8 * __restrict__ src_y,
    guint8 * __restrict__ src_a, gint num_loop)
{
  gint w, i;
  guint alpha, inv_alpha;

  for (w = 0; w < num_loop; w++) {
    for (i = 0; i < 64; i++) {
      if (*src_a != 0) {
        alpha = *src_a + 1;
        inv_alpha = 256 - *src_a;
        *dst_y = ((alpha * *src_y + inv_alpha * *dst_y) / 256);
      }
      dst_y++;
      src_y++;
      src_a++;
    }
  }

}

static void
apply_single (guint8 * dst_y, guint8 * src_y, guint8 * src_a, gint num_loop)
{
  gint w;
  guint alpha, inv_alpha;

  for (w = 0; w < num_loop; w++) {
    if (*src_a != 0) {
      alpha = *src_a + 1;
      inv_alpha = 256 - *src_a;
      *dst_y = ((alpha * *src_y + inv_alpha * *dst_y) / 256);
    }
    dst_y++;
    src_y++;
    src_a++;
  }
}

static void
gst_logo_insert_composition_blend (GstLogoInsertComposition * comp,
    GstVideoFrame * frame)
{
  gint y, u, v;
  GstMapInfo logobuf_map_info, framebuf_map_info;
  guint8 *framebuf_data, *logobuf_data;
  guint8 *dst_a, *dst_y, *dst_uv;
  guint8 *src_a, *src_y, *src_uv;
  gint w, h, width_even;
  gint num_neon_loop, num_single_loop, step_neon;

  GST_DEBUG ("Frame size is %dx%d, compositing a %dx%d logo",
      frame->info.width, frame->info.height, comp->width, comp->height);

  num_neon_loop = round_down (comp->width, 64) / 64;
  num_single_loop = comp->width - round_down (comp->width, 64);

  if (gst_buffer_map (frame->buffer, &framebuf_map_info,
          GST_MAP_WRITE) == FALSE) {
    GST_ERROR_OBJECT (frame->buffer,
        "Failed to map logo buffer data for reading");
    goto out;
  }
  framebuf_data = framebuf_map_info.data;

  if (gst_buffer_map (comp->logo, &logobuf_map_info, GST_MAP_READ) == FALSE) {
    GST_ERROR_OBJECT (comp->logo, "Failed to map logo buffer data for reading");
    goto out;
  }
  logobuf_data = logobuf_map_info.data;

  GST_DEBUG ("Applying Y");

  dst_y = framebuf_data + comp->y * frame->info.width + comp->x;
  src_y = logobuf_data + comp->width * comp->height;
  src_a = logobuf_data;

  step_neon = 64 * num_neon_loop;

  for (h = 0; h < comp->height; h++) {
    apply_neon (dst_y, src_y, src_a, num_neon_loop);
    dst_y += step_neon;
    src_y += step_neon;
    src_a += step_neon;
    apply_single (dst_y, src_y, src_a, num_single_loop);
    src_y += num_single_loop;
    src_a += num_single_loop;
    dst_y += num_single_loop + (frame->info.width - comp->width);
  }

  GST_DEBUG ("Applying UV");

  dst_uv = framebuf_data + frame->info.width * frame->info.height
      + ((comp->y - (comp->y % 2)) * frame->info.width) / 2
      + comp->x - (comp->x % 2);
  src_uv = logobuf_data + 2 * comp->width * comp->height;
  src_a = logobuf_data;
  width_even = comp->width - (comp->width % 2);

  num_neon_loop = round_down (width_even, 64) / 64;
  num_single_loop = width_even - round_down (width_even, 64);
  step_neon = 64 * num_neon_loop;

  for (h = 0; h < comp->height / 2; h++) {
    apply_neon (dst_uv, src_uv, src_a, num_neon_loop);
    dst_uv += step_neon;
    src_uv += step_neon;
    src_a += step_neon;
    apply_single (dst_uv, src_uv, src_a, num_single_loop);
    src_uv += num_single_loop;
    src_a += num_single_loop;
    dst_uv +=
        num_single_loop + (frame->info.width - comp->width + (comp->width % 2));
    src_a += comp->width + (comp->width % 2);
  }
  GST_DEBUG_OBJECT (comp->logo, "Logo successfully applied");
out:
  gst_buffer_unmap (comp->logo, &logobuf_map_info);
  gst_buffer_unmap (frame->buffer, &framebuf_map_info);
}

/* Takes ownership of pixbuf; call with OBJECT_LOCK */
static void
gst_logo_insert_set_pixbuf (GstLogoInsert * overlay, GdkPixbuf * pixbuf)
{
  GstVideoMeta *video_meta;
  guint8 *pixels, *p;
  gint width, height, size;

  if (!gdk_pixbuf_get_has_alpha (pixbuf)) {
    GdkPixbuf *alpha_pixbuf;
    alpha_pixbuf = gdk_pixbuf_add_alpha (pixbuf, FALSE, 0, 0, 0);
    g_object_unref (pixbuf);
    pixbuf = alpha_pixbuf;
  }

  if (overlay->pixbuf != NULL)
    g_object_unref (overlay->pixbuf);
  overlay->pixbuf = g_object_ref (pixbuf);

  overlay->update_composition = TRUE;
}

static gboolean
gst_logo_insert_set_pixbuf_from_location (GstLogoInsert * overlay,
    const gchar * location, GError ** err)
{
  GdkPixbuf *pixbuf;

  pixbuf = gdk_pixbuf_new_from_file (location, err);
  if (pixbuf == NULL)
    return FALSE;

  gst_logo_insert_set_pixbuf (overlay, pixbuf);
  return TRUE;
}

static gboolean
gst_logo_insert_start (GstBaseTransform * trans)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (trans);

  if (overlay->pixbuf != NULL) {
    gst_base_transform_set_passthrough (trans, FALSE);
  } else {
    GST_WARNING_OBJECT (overlay, "no image location set, doing nothing");
    gst_base_transform_set_passthrough (trans, TRUE);
  }

  return TRUE;
}

static gboolean
gst_logo_insert_stop (GstBaseTransform * trans)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (trans);
  return TRUE;
}

static gboolean
gst_logo_insert_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  GST_INFO_OBJECT (filter, "caps: %" GST_PTR_FORMAT, incaps);
  return TRUE;
}

static void
gst_logo_insert_update_composition (GstLogoInsert * overlay)
{
  gint size;
  gint x, y, width, height;
  gint logo_width, logo_height;
  gint sub_x, sub_y;
  GstBuffer *logobuf;
  GdkPixbuf *subpixbuf;
  gint video_width =
      GST_VIDEO_INFO_WIDTH (&GST_VIDEO_FILTER (overlay)->in_info);
  gint video_height =
      GST_VIDEO_INFO_HEIGHT (&GST_VIDEO_FILTER (overlay)->in_info);

  if (overlay->comp) {
    gst_logo_insert_composition_free (overlay->comp);
    overlay->comp = NULL;
  }

  if (overlay->pixbuf == NULL) {
    GST_DEBUG_OBJECT (overlay, "Nothing to compose, passthrough");
    return;
  }

  logo_width = gdk_pixbuf_get_width (overlay->pixbuf);
  logo_height = gdk_pixbuf_get_height (overlay->pixbuf);

  sub_x = ABS (MIN (overlay->offset_x, 0));
  sub_y = ABS (MIN (overlay->offset_y, 0));
  x = MAX (overlay->offset_x, 0);
  y = MAX (overlay->offset_y, 0);
  width =
      MAX (logo_width - sub_x - MAX (overlay->offset_x + logo_width -
          video_width, 0), 0);
  height =
      MAX (logo_height - sub_y - MAX (overlay->offset_y + logo_height -
          video_height, 0), 0);

  if (width == 0 || height == 0) {
    GST_DEBUG_OBJECT (overlay, "Logo is not in the video frame, passthrough");
    return;
  }

  GST_DEBUG_OBJECT (overlay, "Composition: %dx%d onto %dx%d at %d,%d",
      width, height, video_width, video_height, x, y);

  subpixbuf =
      gdk_pixbuf_new_subpixbuf (overlay->pixbuf, sub_x, sub_y, width, height);
  logobuf = gst_logo_insert_convert_pixbuf (subpixbuf);

  overlay->comp =
      gst_logo_insert_composition_new (x, y, width, height, logobuf);
}

static void
gst_logo_insert_before_transform (GstBaseTransform * trans, GstBuffer * outbuf)
{
  GstClockTime stream_time;

  stream_time = gst_segment_to_stream_time (&trans->segment, GST_FORMAT_TIME,
      GST_BUFFER_TIMESTAMP (outbuf));

  if (GST_CLOCK_TIME_IS_VALID (stream_time))
    gst_object_sync_values (GST_OBJECT (trans), stream_time);
}

static GstFlowReturn
gst_logo_insert_transform_frame_ip (GstVideoFilter * filter,
    GstVideoFrame * frame)
{
  GstLogoInsert *overlay = GST_LOGO_INSERT (filter);

  GST_OBJECT_LOCK (overlay);

  if (G_UNLIKELY (overlay->update_composition)) {
    gst_logo_insert_update_composition (overlay);
    overlay->update_composition = FALSE;
  }

  GST_OBJECT_UNLOCK (overlay);

  if (overlay->comp != NULL)
    gst_logo_insert_composition_blend (overlay->comp, frame);

  return GST_FLOW_OK;
}

static gboolean
logoinsert_init (GstPlugin * logoinsert)
{
  return gst_element_register (logoinsert, "logoinsert", GST_RANK_NONE,
      GST_TYPE_LOGO_INSERT);
}

#ifndef PACKAGE
#define PACKAGE "veo-labs"
#endif

#ifndef VERSION
#define VERSION "1.0"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    logoinsert,
    "Logoinsert",
    logoinsert_init, VERSION, "LGPL", "GStreamer", "http://gstreamer.net/")
