/* GStreamer GdkPixbuf overlay
 * Copyright (C) 2012-2014 Tim-Philipp Müller <tim centricular net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_LOGO_INSERT_H_
#define _GST_LOGO_INSERT_H_

#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

#define GST_TYPE_LOGO_INSERT   (gst_logo_insert_get_type())
#define GST_LOGO_INSERT(obj)   (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_LOGO_INSERT,GstLogoInsert))
#define GST_LOGO_INSERT_CLASS(klass)   (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_LOGO_INSERT,GstLogoInsertClass))
#define GST_IS_LOGO_INSERT(obj)   (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_LOGO_INSERT))
#define GST_IS_LOGO_INSERT_CLASS(obj)   (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_LOGO_INSERT))

typedef struct _GstLogoInsert GstLogoInsert;
typedef struct _GstLogoInsertClass GstLogoInsertClass;
typedef struct _GstLogoInsertComposition GstLogoInsertComposition;

/**
 * GstLogoInsertComposition
 */
struct _GstLogoInsertComposition
{
  gint x;
  gint y;
  gint width;
  gint height;
  GstBuffer *logo;
};

/**
 * GstLogoInsert:
 *
 * The opaque element instance structure.
 */
struct _GstLogoInsert
{
  GstVideoFilter               videofilter;

  /* properties */
  gchar                      * location;

  /* pixbuf set via pixbuf property */
  GdkPixbuf                  * pixbuf;

  gint                         offset_x;
  gint                         offset_y;

  GstLogoInsertComposition   * comp;

  /* render position or dimension has changed */
  gboolean                     update_composition;
};

struct _GstLogoInsertClass
{
  GstVideoFilterClass  videofilter_class;
};

GType gst_logo_insert_get_type (void);

G_END_DECLS

#endif
