/* GStreamer
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * gsttstune.c:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
/**
 * SECTION:element-tstune
 * @title: tstune
 *
 * This element is currently able to update buffer timestamps
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "gsttstune.h"

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);


enum
{
  PROP_0,
  PROP_OFFSET,
  PROP_UPDATE_PTS,
  PROP_UPDATE_DTS,
};

#define OFFSET_DEFAULT      0
#define OFFSET_MIN      -2000
#define OFFSET_MAX       2000


#define gst_tstune_parent_class parent_class
G_DEFINE_TYPE (GstTsTune, gst_tstune, GST_TYPE_BASE_TRANSFORM);


static void gst_tstune_finalize (GObject * object);
static void gst_tstune_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_tstune_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstFlowReturn gst_tstune_transform_ip (GstBaseTransform * trans,
   GstBuffer * inbuf);
static gboolean gst_tstune_start (GstBaseTransform * trans);
static gboolean gst_tstune_stop (GstBaseTransform * trans);
static GstStateChangeReturn gst_tstune_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_tstune_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps);
static gboolean gst_tstune_query (GstBaseTransform * base,
    GstPadDirection direction, GstQuery * query);


static void
gst_tstune_finalize (GObject * object)
{
  /* GstTsTune *self; */
  /* self = GST_TSTUNE (object); */

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
gst_tstune_class_init (GstTsTuneClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseTransformClass *gstbasetrans_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = GST_ELEMENT_CLASS (klass);
  gstbasetrans_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_tstune_set_property;
  gobject_class->get_property = gst_tstune_get_property;
  gobject_class->finalize = gst_tstune_finalize;

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_OFFSET, g_param_spec_int ("offset", "offset", "offset",
          OFFSET_MIN, OFFSET_MAX, OFFSET_DEFAULT, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_UPDATE_PTS, g_param_spec_boolean ("update_pts", "update_pts",
          "update_pts", FALSE, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_UPDATE_DTS, g_param_spec_boolean ("update_dts", "update_dts",
          "update_dts", FALSE, G_PARAM_READWRITE));

  gst_element_class_set_static_metadata (gstelement_class,
      "tstune",
      "Generic",
      "Pass data without modification", "Veo-Labs http://www.veo-labs.com/");
  gst_element_class_add_static_pad_template (gstelement_class, &srctemplate);
  gst_element_class_add_static_pad_template (gstelement_class, &sinktemplate);

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_tstune_change_state);

  gstbasetrans_class->transform_ip = GST_DEBUG_FUNCPTR (gst_tstune_transform_ip);
  gstbasetrans_class->start = GST_DEBUG_FUNCPTR (gst_tstune_start);
  gstbasetrans_class->stop = GST_DEBUG_FUNCPTR (gst_tstune_stop);
  gstbasetrans_class->accept_caps =
      GST_DEBUG_FUNCPTR (gst_tstune_accept_caps);
  gstbasetrans_class->query = gst_tstune_query;
}


static void
gst_tstune_init (GstTsTune * self)
{
  gst_base_transform_set_gap_aware (GST_BASE_TRANSFORM_CAST (self), TRUE);

  self->update_pts = FALSE;
  self->update_dts = FALSE;
  self->offset = OFFSET_DEFAULT;
  self->log_properties = TRUE;
}


static void
gst_tstune_update_timestamp (GstTsTune * self, gboolean update, GstClockTime * ts_p, gboolean * drop, const char * ts_name)
{
  GstClockTime ts;
  gint64 offset_ns;

  if (!update)
    return;

  ts = (*ts_p);
  offset_ns = self->offset * 1000000;
  if (! GST_CLOCK_TIME_IS_VALID (ts)) {
    GST_DEBUG_OBJECT (self, "Got invalid %s", ts_name);
    return;
  }

  if ((offset_ns < 0) && ((-offset_ns) > ts)) {
    (*ts_p) = 0; //GST_CLOCK_TIME_NONE;
    GST_DEBUG_OBJECT (self, "Drop buffer because of %s value", ts_name);
    (*drop) = TRUE;
    return;
  }

  (*ts_p) += offset_ns;
}


static GstFlowReturn
gst_tstune_transform_ip (GstBaseTransform * base, GstBuffer * buf)
{
  GstTsTune *self;
  GstFlowReturn flow;
  gint64 offset_ns;
  gboolean drop;

  self = GST_TSTUNE (base);

  if (self->log_properties) {
    GST_DEBUG_OBJECT (self, "update_pts = %d", self->update_pts);
    GST_DEBUG_OBJECT (self, "update_dts = %d", self->update_dts);
    GST_DEBUG_OBJECT (self, "offset = %d", self->offset);
    self->log_properties = FALSE;
  }

  drop = FALSE;
  gst_tstune_update_timestamp(self, self->update_pts, &buf->pts, &drop, "PTS");
  gst_tstune_update_timestamp(self, self->update_dts, &buf->dts, &drop, "DTS");

  if (drop)
    return GST_BASE_TRANSFORM_FLOW_DROPPED;
  else
    return GST_FLOW_OK;
}


static void
gst_tstune_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstTsTune *self;
  self = GST_TSTUNE (object);

  switch (prop_id) {
    case PROP_OFFSET:
      self->offset = g_value_get_int (value);
      break;
    case PROP_UPDATE_PTS:
      self->update_pts = g_value_get_boolean (value);
      break;
    case PROP_UPDATE_DTS:
      self->update_dts = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_tstune_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstTsTune *self;
  self = GST_TSTUNE (object);

  switch (prop_id) {
    case PROP_OFFSET:
      g_value_set_int (value, self->offset);
      break;
    case PROP_UPDATE_PTS:
      g_value_set_boolean (value, self->update_pts);
      break;
    case PROP_UPDATE_DTS:
      g_value_set_boolean (value, self->update_dts);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static gboolean
gst_tstune_start (GstBaseTransform * trans)
{
  /* GstTsTune *self; */
  /* self = GST_TSTUNE (trans); */

  return TRUE;
}


static gboolean
gst_tstune_stop (GstBaseTransform * trans)
{
  GstTsTune *self;
  self = GST_TSTUNE (trans);

  GST_OBJECT_LOCK (self);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}


static gboolean
gst_tstune_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps)
{
  gboolean ret;
  GstPad *pad;

  /* Proxy accept-caps */
  if (direction == GST_PAD_SRC)
    pad = GST_BASE_TRANSFORM_SINK_PAD (base);
  else
    pad = GST_BASE_TRANSFORM_SRC_PAD (base);

  ret = gst_pad_peer_query_accept_caps (pad, caps);

  return ret;
}


static gboolean
gst_tstune_query (GstBaseTransform * base, GstPadDirection direction,
    GstQuery * query)
{
  gboolean ret;

  ret = GST_BASE_TRANSFORM_CLASS (parent_class)->query (base, direction, query);
  return ret;
}


static GstStateChangeReturn
gst_tstune_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstTsTune *self = GST_TSTUNE (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}


static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "tstune", GST_RANK_NONE,
          GST_TYPE_TSTUNE))
    return FALSE;

  return TRUE;
}


#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
  #define VERSION "2.0"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    tstune,
    "Veobox Timestamp Tuner",
    plugin_init,
    VERSION,
    "LGPL",
    "Veo-Labs",
    "http://www.veo-labs.com/")
