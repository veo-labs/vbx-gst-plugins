/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *
 * gsth264tune.h:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#ifndef __GST_H264TUNE_H__
#define __GST_H264TUNE_H__


#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>

G_BEGIN_DECLS


#define GST_TYPE_H264TUNE \
  (gst_h264tune_get_type())
#define GST_H264TUNE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_H264TUNE,GstH264tune))
#define GST_H264TUNE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_H264TUNE,GstH264tuneClass))
#define GST_IS_H264TUNE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_H264TUNE))
#define GST_IS_H264TUNE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_H264TUNE))

typedef struct _GstH264tune GstH264tune;
typedef struct _GstH264tuneClass GstH264tuneClass;

/**
 * GstH264tune:
 *
 * Opaque #GstH264tune data structure
 */
struct _GstH264tune {
  GstBaseTransform 	 element;

  /*< private >*/
  gint prop_crop_flag;
  gint prop_crop_left;
  gint prop_crop_right;
  gint prop_crop_top;
  gint prop_crop_bottom;
  gboolean sps_found;
};

struct _GstH264tuneClass {
  GstBaseTransformClass parent_class;

  /* signals */
  void (*handoff) (GstElement *element, GstBuffer *buf);
};

G_GNUC_INTERNAL GType gst_h264tune_get_type (void);

G_END_DECLS

#endif /* __GST_H264TUNE_H__ */
