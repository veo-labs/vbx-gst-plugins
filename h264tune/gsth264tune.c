/* GStreamer
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * gsth264tune.c:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
/**
 * SECTION:element-h264tune
 * @title: h264tune
 *
 * This element is currently able to modify cropping information in H264
 * streams (H264 NAL SPS)
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "gsth264tune.h"
#include "h264_stream.h"


static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);


enum
{
  PROP_0,
  PROP_CROP_FLAG,
  PROP_CROP_LEFT,
  PROP_CROP_RIGHT,
  PROP_CROP_BOTTOM,
  PROP_CROP_TOP,
};

#define DEFAULT_PROP_CROP_FLAG    -1 /* no change */
#define DEFAULT_PROP_CROP_LEFT    0
#define DEFAULT_PROP_CROP_RIGHT   0
#define DEFAULT_PROP_CROP_BOTTOM  0
#define DEFAULT_PROP_CROP_TOP     0


#define SPS_MAX_SIZE 32

#define gst_h264tune_parent_class parent_class
G_DEFINE_TYPE (GstH264tune, gst_h264tune, GST_TYPE_BASE_TRANSFORM);


static void gst_h264tune_finalize (GObject * object);
static void gst_h264tune_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_h264tune_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstFlowReturn gst_h264tune_transform_ip (GstBaseTransform * trans,
   GstBuffer * inbuf);
static gboolean gst_h264tune_start (GstBaseTransform * trans);
static gboolean gst_h264tune_stop (GstBaseTransform * trans);
static GstStateChangeReturn gst_h264tune_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_h264tune_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps);
static gboolean gst_h264tune_query (GstBaseTransform * base,
    GstPadDirection direction, GstQuery * query);


static void
gst_h264tune_finalize (GObject * object)
{
  /* GstH264tune *self; */
  /* self = GST_H264TUNE (object); */

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
gst_h264tune_class_init (GstH264tuneClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseTransformClass *gstbasetrans_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = GST_ELEMENT_CLASS (klass);
  gstbasetrans_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_h264tune_set_property;
  gobject_class->get_property = gst_h264tune_get_property;
  gobject_class->finalize = gst_h264tune_finalize;

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_CROP_FLAG, g_param_spec_int ("crop_flag", "crop_flag", "crop_flag",
          -1, 1, DEFAULT_PROP_CROP_FLAG, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_CROP_LEFT, g_param_spec_int ("crop_left", "crop_left", "crop_left",
          0, G_MAXINT, DEFAULT_PROP_CROP_LEFT, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_CROP_RIGHT, g_param_spec_int ("crop_right", "crop_right", "crop_right",
          0, G_MAXINT, DEFAULT_PROP_CROP_RIGHT, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_CROP_TOP, g_param_spec_int ("crop_top", "crop_top", "crop_top",
          0, G_MAXINT, DEFAULT_PROP_CROP_TOP, G_PARAM_READWRITE));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_CROP_BOTTOM, g_param_spec_int ("crop_bottom", "crop_bottom", "crop_bottom",
          0, G_MAXINT, DEFAULT_PROP_CROP_BOTTOM, G_PARAM_READWRITE));

  gst_element_class_set_static_metadata (gstelement_class,
      "H264tune",
      "Generic",
      "Pass data without modification", "Veo-Labs http://www.veo-labs.com/");
  gst_element_class_add_static_pad_template (gstelement_class, &srctemplate);
  gst_element_class_add_static_pad_template (gstelement_class, &sinktemplate);

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_h264tune_change_state);

  gstbasetrans_class->transform_ip = GST_DEBUG_FUNCPTR (gst_h264tune_transform_ip);
  gstbasetrans_class->start = GST_DEBUG_FUNCPTR (gst_h264tune_start);
  gstbasetrans_class->stop = GST_DEBUG_FUNCPTR (gst_h264tune_stop);
  gstbasetrans_class->accept_caps =
      GST_DEBUG_FUNCPTR (gst_h264tune_accept_caps);
  gstbasetrans_class->query = gst_h264tune_query;
}


static void
gst_h264tune_init (GstH264tune * self)
{
  gst_base_transform_set_gap_aware (GST_BASE_TRANSFORM_CAST (self), TRUE);

  self->prop_crop_flag = DEFAULT_PROP_CROP_FLAG;
  self->prop_crop_left = DEFAULT_PROP_CROP_LEFT;
  self->prop_crop_right = DEFAULT_PROP_CROP_RIGHT;
  self->prop_crop_top = DEFAULT_PROP_CROP_TOP;
  self->prop_crop_bottom = DEFAULT_PROP_CROP_BOTTOM;
  self->sps_found = FALSE;
}


typedef struct gst_h264tune_buf_rewrite_segment_struct
{
  int start;
  int size;
  uint8_t *new_data;
  uint8_t *new_free;
  int new_size;
} gst_h264tune_buf_rewrite_segment_t;


static GstFlowReturn
gst_h264tune_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstH264tune *self = GST_H264TUNE (trans);
  GstMapInfo map;
  h264_stream_t* h;
  gboolean mapped;
  int nal_start, nal_end, nal_size;
  int in_size;
  int out_size;
  uint8_t * inp;
  uint8_t * outp;
  uint8_t * ptr;
  GList * segments;
  GList * it;
  gst_h264tune_buf_rewrite_segment_t * seg;
  int i, j, k;
  const char * error_msg;
  GstBuffer * outbuf;
  GstMemory * outmem;

  h = NULL;
  error_msg = NULL;
  mapped = FALSE;
  outbuf = NULL;

  segments = NULL;
  if (self->sps_found)
    goto end;

  if (!gst_buffer_map (buf, &map, GST_MAP_READ))
    goto end;

  mapped = TRUE;
  h = h264_new();
  if (h==NULL)
    goto end;

  /* check if nals must be re-written */
  inp = map.data;
  in_size = gst_buffer_get_size (buf);
  while (find_nal_unit(inp, in_size, &nal_start, &nal_end) > 0) {
    ptr = inp + nal_start;
    nal_start = inp - map.data + nal_start;
    nal_end = inp - map.data + nal_end;
    inp += nal_end;
    nal_size = nal_end - nal_start;
    if (nal_size > SPS_MAX_SIZE)
      continue;
    read_nal_unit(h, ptr, nal_size);
    if (h->nal->nal_unit_type != NAL_UNIT_TYPE_SPS)
      continue;

    self->sps_found = TRUE;
    if (self->prop_crop_flag==0)
      h->sps->frame_cropping_flag = 0;

    else if (self->prop_crop_flag==1) {
      h->sps->frame_cropping_flag = 1;
      h->sps->frame_crop_left_offset = self->prop_crop_left;
      h->sps->frame_crop_right_offset = self->prop_crop_right;
      h->sps->frame_crop_top_offset = self->prop_crop_top;
      h->sps->frame_crop_bottom_offset = self->prop_crop_bottom;
    }

    else
      break;

    seg = g_malloc (sizeof(gst_h264tune_buf_rewrite_segment_t));
    seg->start = nal_start;
    seg->size = nal_size;
    seg->new_free = g_malloc(SPS_MAX_SIZE);
    seg->new_size = write_nal_unit(h, seg->new_free, SPS_MAX_SIZE);
    if (seg->new_size < 0) {
      error_msg = "write_nal_unit() failed => SPS crop flag unchanged";
      segments = g_list_append (segments, seg);
      break;
    }
    seg->new_data = seg->new_free + 1;
    seg->new_size--;
    segments = g_list_append (segments, seg);
  }
  if ((segments == NULL) || error_msg)
    goto end;

  /* rewrite buffer */
  inp = map.data;
  out_size = in_size;
  for (it = segments; it; it = it->next) {
    seg = it->data;
    out_size -= seg->size;
    if (seg->new_data)
      out_size += seg->new_size;
  }
  outp = g_malloc0 (out_size);
  seg = g_list_nth_data (segments, 0);
  i=0;
  j=0;
  it = segments;
  for (;;) {
    if (it==NULL) {
      for (; j<out_size; i++, j++)
        outp[j] = inp[i];
      break;
    }

    seg = it->data;
    if (i < seg->start) {
      for (; i < seg->start; i++, j++)
        outp[j] = inp[i];
    }

    else if (i == seg->start) {
      if (seg->new_data) {
        for (k=0; k < seg->new_size; j++, k++)
          outp[j] = seg->new_data[k];
      }
      i += seg->size;
      it = it->next;
    }
  }
  outbuf = gst_buffer_new_wrapped (outp, out_size);

  /* finalize */
end:
  if (segments) {
    for (it = segments; it; it = it->next) {
      if (seg->new_free)
        g_free (seg->new_free);
      else if (seg->new_data)
        g_free (seg->new_data);
      g_free (seg);
    }
    g_list_free (segments);
  }

  if (outbuf) {
    outmem = gst_buffer_get_memory (outbuf, 0);
    gst_buffer_replace_all_memory (buf, outmem);
    gst_buffer_unref (outbuf);
  }

  if (mapped)
    gst_buffer_unmap (buf, &map);

  if (h)
    h264_free(h);

  if (error_msg)
    GST_ERROR_OBJECT (self, error_msg);

  return ret;
}


static void
gst_h264tune_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstH264tune *self;
  self = GST_H264TUNE (object);

  switch (prop_id) {
    case PROP_CROP_FLAG:
      self->prop_crop_flag = g_value_get_int (value);
      break;
    case PROP_CROP_LEFT:
      self->prop_crop_left = g_value_get_int (value);
      break;
    case PROP_CROP_RIGHT:
      self->prop_crop_right = g_value_get_int (value);
      break;
    case PROP_CROP_TOP:
      self->prop_crop_top = g_value_get_int (value);
      break;
    case PROP_CROP_BOTTOM:
      self->prop_crop_bottom = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_h264tune_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstH264tune *self;
  self = GST_H264TUNE (object);

  switch (prop_id) {
    case PROP_CROP_FLAG:
      g_value_set_int (value, self->prop_crop_flag);
      break;
    case PROP_CROP_LEFT:
      g_value_set_int (value, self->prop_crop_left);
      break;
    case PROP_CROP_RIGHT:
      g_value_set_int (value, self->prop_crop_right);
      break;
    case PROP_CROP_TOP:
      g_value_set_int (value, self->prop_crop_top);
      break;
    case PROP_CROP_BOTTOM:
      g_value_set_int (value, self->prop_crop_bottom);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static gboolean
gst_h264tune_start (GstBaseTransform * trans)
{
  /* GstH264tune *self; */
  /* self = GST_H264TUNE (trans); */

  return TRUE;
}


static gboolean
gst_h264tune_stop (GstBaseTransform * trans)
{
  GstH264tune *self;
  self = GST_H264TUNE (trans);

  GST_OBJECT_LOCK (self);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}


static gboolean
gst_h264tune_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps)
{
  gboolean ret;
  GstPad *pad;

  /* Proxy accept-caps */

  if (direction == GST_PAD_SRC)
    pad = GST_BASE_TRANSFORM_SINK_PAD (base);
  else
    pad = GST_BASE_TRANSFORM_SRC_PAD (base);

  ret = gst_pad_peer_query_accept_caps (pad, caps);

  return ret;
}


static gboolean
gst_h264tune_query (GstBaseTransform * base, GstPadDirection direction,
    GstQuery * query)
{
  gboolean ret;
  /* GstH264tune *self; */
  /* self = GST_H264TUNE (base); */

  ret = GST_BASE_TRANSFORM_CLASS (parent_class)->query (base, direction, query);
  return ret;
}


static GstStateChangeReturn
gst_h264tune_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstH264tune *self = GST_H264TUNE (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}


static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "h264tune", GST_RANK_NONE,
          GST_TYPE_H264TUNE))
    return FALSE;

  return TRUE;
}



#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
  #define VERSION "2.0"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    h264tune,
    "Tune h264 stream",
    plugin_init,
    VERSION,
    "LGPL",
    "Veo-Labs",
    "http://www.veo-labs.com/")
