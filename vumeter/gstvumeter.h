/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2000 Wim Taymans <wtay@chello.be>
 *
 * gstvumeter.h:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#ifndef __GST_VUMETER_H__
#define __GST_VUMETER_H__


#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>

G_BEGIN_DECLS


#define GST_TYPE_VUMETER \
  (gst_vumeter_get_type())
#define GST_VUMETER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_VUMETER,GstVumeter))
#define GST_VUMETER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_VUMETER,GstVumeterClass))
#define GST_IS_VUMETER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_VUMETER))
#define GST_IS_VUMETER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_VUMETER))

typedef struct _GstVumeter GstVumeter;
typedef struct _GstVumeterClass GstVumeterClass;

/**
 * GstVumeter:
 *
 * Opaque #GstVumeter data structure
 */
struct _GstVumeter {
  GstBaseTransform 	 element;
};

struct _GstVumeterClass {
  GstBaseTransformClass parent_class;
};

G_GNUC_INTERNAL GType gst_vumeter_get_type (void);

G_END_DECLS

#endif /* __GST_VUMETER_H__ */
