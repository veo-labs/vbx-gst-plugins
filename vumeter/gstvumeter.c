/* GStreamer
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * gstvumeter.c:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
/**
 * SECTION:element-vumeter
 * @title: vumeter
 *
 * This element is currently able to modify cropping information in H264
 * streams (H264 NAL SBS)
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include <vbx-libvumeter/vumeter.h>

#include "gstvumeter.h"

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);


enum
{
  PROP_0,
};


#define gst_vumeter_parent_class parent_class
G_DEFINE_TYPE (GstVumeter, gst_vumeter, GST_TYPE_BASE_TRANSFORM);


static void gst_vumeter_finalize (GObject * object);
static void gst_vumeter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_vumeter_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstFlowReturn gst_vumeter_transform_ip (GstBaseTransform * trans,
   GstBuffer * inbuf);
static gboolean gst_vumeter_start (GstBaseTransform * trans);
static gboolean gst_vumeter_stop (GstBaseTransform * trans);
static GstStateChangeReturn gst_vumeter_change_state (GstElement * element,
    GstStateChange transition);
static gboolean gst_vumeter_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps);
static gboolean gst_vumeter_query (GstBaseTransform * base,
    GstPadDirection direction, GstQuery * query);


static void
gst_vumeter_finalize (GObject * object)
{
  /* GstVumeter *self; */
  /* self = GST_VUMETER (object); */

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
gst_vumeter_class_init (GstVumeterClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseTransformClass *gstbasetrans_class;

  gobject_class = G_OBJECT_CLASS (klass);
  gstelement_class = GST_ELEMENT_CLASS (klass);
  gstbasetrans_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_vumeter_set_property;
  gobject_class->get_property = gst_vumeter_get_property;
  gobject_class->finalize = gst_vumeter_finalize;

  gst_element_class_set_static_metadata (gstelement_class,
      "Vumeter",
      "Generic",
      "Pass data without modification", "Veo-Labs http://www.veo-labs.com/");
  gst_element_class_add_static_pad_template (gstelement_class, &srctemplate);
  gst_element_class_add_static_pad_template (gstelement_class, &sinktemplate);

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_vumeter_change_state);

  gstbasetrans_class->transform_ip = GST_DEBUG_FUNCPTR (gst_vumeter_transform_ip);
  gstbasetrans_class->start = GST_DEBUG_FUNCPTR (gst_vumeter_start);
  gstbasetrans_class->stop = GST_DEBUG_FUNCPTR (gst_vumeter_stop);
  gstbasetrans_class->accept_caps =
      GST_DEBUG_FUNCPTR (gst_vumeter_accept_caps);
  gstbasetrans_class->query = gst_vumeter_query;
}


static void
gst_vumeter_init (GstVumeter * self)
{
  gst_base_transform_set_gap_aware (GST_BASE_TRANSFORM_CAST (self), TRUE);
}

static GstFlowReturn
gst_vumeter_transform_ip (GstBaseTransform * trans, GstBuffer * buf)
{
  GstVumeter *self;
  int size;
  GstMapInfo map;
  gboolean mapped;
  GstFlowReturn flow;

  self = GST_VUMETER (trans);
  mapped = FALSE;
  size = gst_buffer_get_size (buf);
  if (!gst_buffer_map (buf, &map, GST_MAP_READ))
    goto end;
  mapped = TRUE;

  vumeter_feed (map.data, size);

 end:
  if (mapped)
    gst_buffer_unmap (buf, &map);

  return GST_FLOW_OK;
}


static void
gst_vumeter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstVumeter *self;
  self = GST_VUMETER (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_vumeter_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstVumeter *self;
  self = GST_VUMETER (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static gboolean
gst_vumeter_start (GstBaseTransform * trans)
{
  /* GstVumeter *self; */
  /* self = GST_VUMETER (trans); */

  return TRUE;
}


static gboolean
gst_vumeter_stop (GstBaseTransform * trans)
{
  GstVumeter *self;
  self = GST_VUMETER (trans);

  vumeter_set_state(VUMETER_STATE_PAUSE);

  GST_OBJECT_LOCK (self);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}


static gboolean
gst_vumeter_accept_caps (GstBaseTransform * base,
    GstPadDirection direction, GstCaps * caps)
{
  gboolean ret;
  GstPad *pad;

  /* Proxy accept-caps */

  if (direction == GST_PAD_SRC)
    pad = GST_BASE_TRANSFORM_SINK_PAD (base);
  else
    pad = GST_BASE_TRANSFORM_SRC_PAD (base);

  ret = gst_pad_peer_query_accept_caps (pad, caps);

  return ret;
}


static gboolean
gst_vumeter_query (GstBaseTransform * base, GstPadDirection direction,
    GstQuery * query)
{
  gboolean ret;
  /* GstVumeter *self; */
  /* self = GST_VUMETER (base); */

  ret = GST_BASE_TRANSFORM_CLASS (parent_class)->query (base, direction, query);
  return ret;
}


static GstStateChangeReturn
gst_vumeter_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  GstVumeter *self = GST_VUMETER (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      GST_OBJECT_LOCK (self);
      GST_OBJECT_UNLOCK (self);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}


static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "vumeter", GST_RANK_NONE,
          GST_TYPE_VUMETER))
    return FALSE;

  return TRUE;
}



#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
  #define VERSION "2.0"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    vumeter,
    "Veobox Vumeter",
    plugin_init,
    VERSION,
    "LGPL",
    "Veo-Labs",
    "http://www.veo-labs.com/")
