#include "fboptsink.h"
#include "fboptsink_neon.h"

void OPTIMIZE
fboptsink_blit_hd_frame(fbshape_t * __restrict dst, yshape_t * __restrict y_src, uvshape_t * __restrict uv_src)
{
  int i, j, m, n;
  uint8x16x2_t y0 BUF_ALIGN;
  uint8x16x2_t y1 BUF_ALIGN;
  uint8x16x2_t y2 BUF_ALIGN;
  uint8x16x2_t y3 BUF_ALIGN;
  uint8x16x2_t uvA BUF_ALIGN;
  uint8x16x2_t uvB BUF_ALIGN;
  uint8x16_t avg0 BUF_ALIGN;
  uint8x16_t avg1 BUF_ALIGN;
  uint8x16_t yA BUF_ALIGN;
  uint8x16_t yB BUF_ALIGN;
  uint16x8x2_t rgbA, rgbB;

  for (i=0, m=0; i<HD_HEIGHT; i+=4, m+=2) {
    for (j=0, n=0; j<HD_WIDTH; j+=(16*2), n+=16) {
      y0 = vld2q_u8(&y_src->hd[i+0][j]);
      y1 = vld2q_u8(&y_src->hd[i+1][j]);
      y2 = vld2q_u8(&y_src->hd[i+2][j]);
      y3 = vld2q_u8(&y_src->hd[i+3][j]);

      uvA = vld2q_u8(&uv_src->hd[m+0][j]);
      uvB = vld2q_u8(&uv_src->hd[m+1][j]);

      avg0 = vhaddq_u8(y0.val[0], y0.val[1]);
      avg1 = vhaddq_u8(y1.val[0], y1.val[1]);
      yA = vhaddq_u8(avg0, avg1);
      avg0 = vhaddq_u8(y2.val[0], y2.val[1]);
      avg1 = vhaddq_u8(y3.val[0], y3.val[1]);
      yB = vhaddq_u8(avg0, avg1);

      rgbA = fboptsink_convert_yuv888x16_to_rgb16x16(yA, uvA.val[0], uvA.val[1]);
      rgbB = fboptsink_convert_yuv888x16_to_rgb16x16(yB, uvB.val[0], uvB.val[1]);

      vst1q_u16(&dst->fb[FB_POSY+m+0][FB_POSX+n+0], rgbA.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+m+0][FB_POSX+n+8], rgbA.val[1]);
      vst1q_u16(&dst->fb[FB_POSY+m+1][FB_POSX+n+0], rgbB.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+m+1][FB_POSX+n+8], rgbB.val[1]);
    }
  }
}
