#include "fboptsink.h"
#include "fboptsink_neon.h"


void OPTIMIZE
fboptsink_blit_sd_frame(fbshape_t * __restrict dst, yshape_t * __restrict y_src, uvshape_t * __restrict uv_src)
{
  int i, j;
  uint8x16_t y0 BUF_ALIGN;
  uint8x16_t y1 BUF_ALIGN;
  uint8x16_t y2 BUF_ALIGN;
  uint8x16_t y3 BUF_ALIGN;
  uint8x16x2_t uv BUF_ALIGN;
  uint8x16x2_t u BUF_ALIGN;
  uint8x16x2_t v BUF_ALIGN;
  uint16x8x2_t rgb0, rgb1, rgb2, rgb3;

  for (i=0; i<SD_HEIGHT; i+=2) {
    for (j=0; j<SD_WIDTH; j+=(16*2)) {
      y0 = vld1q_u8(&y_src->sd[i][j]);
      y1 = vld1q_u8(&y_src->sd[i][j+16]);
      y2 = vld1q_u8(&y_src->sd[i+1][j]);
      y3 = vld1q_u8(&y_src->sd[i+1][j+16]);
      uv = vld2q_u8(&uv_src->sd[i>>1][j]);

      u = vzipq_u8 (uv.val[0], uv.val[0]);
      v = vzipq_u8 (uv.val[1], uv.val[1]);

      rgb0 = fboptsink_convert_yuv888x16_to_rgb16x16(y0, u.val[0], v.val[0]);
      rgb1 = fboptsink_convert_yuv888x16_to_rgb16x16(y1, u.val[1], v.val[1]);
      rgb2 = fboptsink_convert_yuv888x16_to_rgb16x16(y2, u.val[0], v.val[0]);
      rgb3 = fboptsink_convert_yuv888x16_to_rgb16x16(y3, u.val[1], v.val[1]);

      vst1q_u16(&dst->fb[FB_POSY+i][FB_POSX+j], rgb0.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+i][FB_POSX+j+8], rgb0.val[1]);
      vst1q_u16(&dst->fb[FB_POSY+i][FB_POSX+j+16], rgb1.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+i][FB_POSX+j+24], rgb1.val[1]);
      vst1q_u16(&dst->fb[FB_POSY+i+1][FB_POSX+j], rgb2.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+i+1][FB_POSX+j+8], rgb2.val[1]);
      vst1q_u16(&dst->fb[FB_POSY+i+1][FB_POSX+j+16], rgb3.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+i+1][FB_POSX+j+24], rgb3.val[1]);
    }
  }
}
