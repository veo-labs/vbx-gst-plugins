#include "fboptsink.h"
#include "fboptsink_neon.h"

#include "fboptsink.h"
#include "fboptsink_neon.h"


//#define DUMMY_FRAME
#ifdef DUMMY_FRAME
static void
make_dummy_frame (yshape_t * __restrict* y_data, uvshape_t *__restrict* uv_data)
{
  static uint8_t y[FHD_HEIGHT][FHD_WIDTH];
  static uint8_t uv[FHD_HEIGHT/2][FHD_WIDTH];
  int i, j, val;
  uint8_t valm;

  val = 0;
  for (i=0; i<FHD_HEIGHT; i++) {
    for (j=0; j<FHD_WIDTH; j++) {
      valm = val % 256;
      y[i][j] = valm;
      val++;
    }
    val += 79;
  }

  val = 100;
  for (i=0; i<(FHD_HEIGHT/2); i++) {
    for (j=0; j<FHD_WIDTH; j++) {
      valm = val % 256;
      uv[i][j] = valm;
      val++;
    }
    val += 79;
  }

  (*y_data) = (yshape_t *)y;
  (*uv_data) = (uvshape_t *)uv;
  raise(5);
}
#endif


static const uint8_t UV_INDEXES[] = {0, 12, 1, 13, 3, 15, 5, 17, 6, 18, 7, 19, 9, 21, 11, 23};



void OPTIMIZE
fboptsink_blit_fhd_frame (fbshape_t * __restrict dst, yshape_t * __restrict y_src, uvshape_t * __restrict uv_src)
{
  int i, j, m, n, k;

  uint8x16x3_t y0 BUF_ALIGN;
  uint8x16x3_t y1 BUF_ALIGN;
  uint8x16x3_t y2 BUF_ALIGN;
  uint8x16x3_t y3 BUF_ALIGN;
  uint8x16_t avg0 BUF_ALIGN;
  uint8x16_t avg1 BUF_ALIGN;
  uint8x16_t yA BUF_ALIGN;
  uint8x16_t yB BUF_ALIGN;
  uint8x8x2_t uv0 BUF_ALIGN;
  uint8x8x2_t uv1 BUF_ALIGN;
  uint8x8x2_t uv2 BUF_ALIGN;
  uint8x8x2_t UVX BUF_ALIGN;
  uint8x8x3_t tt BUF_ALIGN;
  uint8x8x2_t td BUF_ALIGN;
  uint8x16_t u BUF_ALIGN;
  uint8x16_t v BUF_ALIGN;
  uint16x8x2_t rgbA, rgbB;

#ifdef DUMMY_FRAME
  make_dummy_frame (&y_src, &uv_src);
#endif

  UVX = vld2_u8(UV_INDEXES);
  for (i=0, m=0, k=0; i<FHD_HEIGHT; i+=6, m+=2, k+=3) {
    for (j=0, n=0; j<FHD_WIDTH; j+=(16*3), n+=16) {
      y0 = vld3q_u8(&y_src->fhd[i+0][j]);
      y1 = vld3q_u8(&y_src->fhd[i+2][j]);
      y2 = vld3q_u8(&y_src->fhd[i+3][j]);
      y3 = vld3q_u8(&y_src->fhd[i+5][j]);

      uv0 = vld2_u8(&uv_src->fhd[k+1][j+0]);
      uv1 = vld2_u8(&uv_src->fhd[k+1][j+16]);
      uv2 = vld2_u8(&uv_src->fhd[k+1][j+32]);

      avg0 = vhaddq_u8(y0.val[0], y0.val[2]);
      avg1 = vhaddq_u8(y1.val[0], y1.val[2]);
      yA = vhaddq_u8(avg0, avg1);
      avg0 = vhaddq_u8(y2.val[0], y2.val[2]);
      avg1 = vhaddq_u8(y3.val[0], y3.val[2]);
      yB = vhaddq_u8(avg0, avg1);

      tt.val[0] = uv0.val[0];
      tt.val[1] = uv1.val[0];
      tt.val[2] = uv2.val[0];
      td.val[0] = vtbl3_u8 (tt, UVX.val[0]);
      td.val[1] = vtbl3_u8 (tt, UVX.val[1]);
      u = vcombine_u8(td.val[0], td.val[1]);

      tt.val[0] = uv0.val[1];
      tt.val[1] = uv1.val[1];
      tt.val[2] = uv2.val[1];
      td.val[0] = vtbl3_u8 (tt, UVX.val[0]);
      td.val[1] = vtbl3_u8 (tt, UVX.val[1]);
      v = vcombine_u8(td.val[0], td.val[1]);

      rgbA = fboptsink_convert_yuv888x16_to_rgb16x16(yA, u, v);
      rgbB = fboptsink_convert_yuv888x16_to_rgb16x16(yB, u, v);

      vst1q_u16(&dst->fb[FB_POSY+m][FB_POSX+n], rgbA.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+m][FB_POSX+n+8], rgbA.val[1]);
      vst1q_u16(&dst->fb[FB_POSY+m+1][FB_POSX+n], rgbB.val[0]);
      vst1q_u16(&dst->fb[FB_POSY+m+1][FB_POSX+n+8], rgbB.val[1]);
    }
  }
}
