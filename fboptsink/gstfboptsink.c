#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <stdint.h>

#include "gstfboptsink.h"
#include "fboptsink.h"

static GstFlowReturn gst_fboptsink_show_frame (GstVideoSink * videosink,
    GstBuffer * buff);

static gboolean gst_fboptsink_start (GstBaseSink * bsink);
static gboolean gst_fboptsink_stop (GstBaseSink * bsink);

static GstCaps *gst_fboptsink_getcaps (GstBaseSink * bsink, GstCaps * filter);
static gboolean gst_fboptsink_setcaps (GstBaseSink * bsink, GstCaps * caps);

static void gst_fboptsink_finalize (GObject * object);
static void gst_fboptsink_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_fboptsink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static GstStateChangeReturn gst_fboptsink_change_state (GstElement * element,
    GstStateChange transition);


static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ NV12 }"))
    );

#define parent_class gst_fboptsink_parent_class
G_DEFINE_TYPE (GstFBOPTSink, gst_fboptsink, GST_TYPE_VIDEO_SINK);


#define DEFAULT_ARG_FORCE_NOP FALSE

enum
{
  ARG_0,
  ARG_FORCE_NOP,
};


static void
gst_fboptsink_init (GstFBOPTSink * self)
{
  self->blit_func = NULL;
  self->force_nop = DEFAULT_ARG_FORCE_NOP;
  self->count = -1;
}


void
gst_fboptsink_clear_fb (GstFBOPTSink * self, int fromline)
{
  int i, j;
  uint16_t * fb_mem = (uint16_t *)self->fb_mem;
  uint16_t line[FB_WIDTH];

  for (i=0; i<FB_HEIGHT; i++) {
    if (i < fromline)
      continue;
    for (j=0; j<FB_WIDTH; j++) {
      line[j] = 0x0000;
    }
    memcpy (fb_mem + (i * FB_WIDTH), line, sizeof(line));
  }
}


static GstCaps *
gst_fboptsink_getcaps (GstBaseSink * bsink, GstCaps * filter)
{
  GstFBOPTSink *self;
  GstCaps *caps;

  self = GST_FBOPTSINK (bsink);
  caps = gst_static_pad_template_get_caps (&sink_template);
  return caps;
}


static gboolean
gst_fboptsink_setcaps (GstBaseSink * bsink, GstCaps * vscapslist)
{
  GstFBOPTSink *self;
  GstStructure *structure;
  const GValue *fps;
  int width;
  int height;
  gboolean bblit;

  self = GST_FBOPTSINK (bsink);

  bblit = !self->force_nop;
  if (g_file_test ("/var/run/fboptsink-bliton", G_FILE_TEST_EXISTS))
    bblit = TRUE;
  if (g_file_test ("/var/run/fboptsink-blitoff", G_FILE_TEST_EXISTS))
    bblit = FALSE;

  structure = gst_caps_get_structure (vscapslist, 0);
  gst_structure_get_int (structure, "width", &width);
  gst_structure_get_int (structure, "height", &height);

  self->uv_offset = width * height;
  
  if (!bblit) {
    self->blit_func = NULL;
    return TRUE;
  }
  else {
    if (width==FHD_WIDTH)
      self->blit_func = (void *)fboptsink_blit_fhd_frame;

    else if (width==HD_WIDTH)
      self->blit_func = (void *)fboptsink_blit_hd_frame;

    else if (width==SD_WIDTH)
      self->blit_func = (void *)fboptsink_blit_sd_frame;

    else
      self->blit_func = (void *)fboptsink_blit_error_frame;

    return TRUE;
  }
}


static GstFlowReturn
gst_fboptsink_show_frame (GstVideoSink * videosink, GstBuffer * buf)
{
  GstFBOPTSink *self;
  GstMapInfo map;
  GstFlowReturn flow;
  fbshape_t * dst;
  yshape_t * y_src;
  uvshape_t * uv_src;
  gboolean unmap;
  blit_func_t blit_func;

  flow = GST_FLOW_ERROR;
  unmap = FALSE;
  self = GST_FBOPTSINK (videosink);

  if (!self->blit_func) {
    flow = GST_FLOW_OK;
    goto end;
  }

  self->count = (self->count+1) % 2;
  if (self->count==0) {
    flow = GST_FLOW_OK;
    goto end;
  }

  if (!gst_buffer_map (buf, &map, GST_MAP_READ))
    goto end;

  unmap = TRUE;
  flow = GST_FLOW_ERROR;
  dst = (fbshape_t *)self->fb_mem;
  y_src = (yshape_t *)map.data;
  uv_src = (uvshape_t *)(map.data + self->uv_offset);

  if (!self->fb_cleared) {
    gst_fboptsink_clear_fb (self, FB_POSY);
    self->fb_cleared = TRUE;
  }

  blit_func = (blit_func_t)self->blit_func;
  blit_func(dst,y_src,uv_src);
  flow = GST_FLOW_OK;

end:
  if (unmap)
    gst_buffer_unmap (buf, &map);
  return flow;
}


static gboolean
gst_fboptsink_start (GstBaseSink * bsink)
{
  GstFBOPTSink *self;

  self = GST_FBOPTSINK (bsink);

  self->fd = open ("/dev/fb0", O_RDWR);

  if (self->fd == -1)
    return FALSE;

  /* map the framebuffer */
  self->fb_mem = mmap (0, FB_WIDTH*FB_HEIGHT*2,
      PROT_WRITE, MAP_SHARED, self->fd, 0);
  if (self->fb_mem == MAP_FAILED)
    return FALSE;

  self->fb_cleared = FALSE;
  
  return TRUE;
}


static gboolean
gst_fboptsink_stop (GstBaseSink * bsink)
{
  GstFBOPTSink *self;

  self = GST_FBOPTSINK (bsink);

  if (munmap (self->fb_mem, FB_WIDTH*FB_HEIGHT*2))
    return FALSE;

  if (close (self->fd))
    return FALSE;

  return TRUE;
}


static void
gst_fboptsink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFBOPTSink *self;

  self = GST_FBOPTSINK (object);

  switch (prop_id) {
    case ARG_FORCE_NOP:{
      gboolean b;
      b = g_value_get_boolean (value);
      self->force_nop = b;
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_fboptsink_get_property (GObject * object, guint prop_id, GValue * value,
    GParamSpec * pspec)
{
  GstFBOPTSink *self;

  self = GST_FBOPTSINK (object);

  switch (prop_id) {
    case ARG_FORCE_NOP:{
      g_value_set_boolean (value, self->force_nop);
      break;
    }
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static GstStateChangeReturn
gst_fboptsink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  g_return_val_if_fail (GST_IS_FBOPTSINK (element), GST_STATE_CHANGE_FAILURE);

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    default:
      break;
  }
  return ret;
}


static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "fboptsink", GST_RANK_NONE,
          GST_TYPE_FBOPTSINK))
    return FALSE;

  return TRUE;
}


static void
gst_fboptsink_class_init (GstFBOPTSinkClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSinkClass *basesink_class;
  GstVideoSinkClass *videosink_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  basesink_class = (GstBaseSinkClass *) klass;
  videosink_class = (GstVideoSinkClass *) klass;

  gobject_class->set_property = gst_fboptsink_set_property;
  gobject_class->get_property = gst_fboptsink_get_property;
  gobject_class->finalize = gst_fboptsink_finalize;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_fboptsink_change_state);

  basesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_fboptsink_setcaps);
  basesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_fboptsink_getcaps);
  basesink_class->start = GST_DEBUG_FUNCPTR (gst_fboptsink_start);
  basesink_class->stop = GST_DEBUG_FUNCPTR (gst_fboptsink_stop);

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      ARG_FORCE_NOP, g_param_spec_boolean ("force_nop",
          "Force non-operation", "Force non-operation",
          DEFAULT_ARG_FORCE_NOP, G_PARAM_READWRITE));
  
  videosink_class->show_frame = GST_DEBUG_FUNCPTR (gst_fboptsink_show_frame);

  gst_element_class_set_static_metadata (gstelement_class, "fbopt video sink",
      "Sink/Video", "Veobox framebuffer videosink",
      "Veo-Labs http://www.veo-labs.com/");

  gst_element_class_add_static_pad_template (gstelement_class, &sink_template);
}


static void
gst_fboptsink_finalize (GObject * object)
{
  GstFBOPTSink *self = GST_FBOPTSINK (object);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}


#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
  #define VERSION "2.0"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    fboptsink,
    "Veobox framebuffer video sink",
    plugin_init,
    VERSION,
    "LGPL",
    "Veo-Labs",
    "http://www.veo-labs.com/")
