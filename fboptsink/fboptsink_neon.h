#include <arm_neon.h>

#ifndef __FBOPTSINK_NEON_H__
#define __FBOPTSINK_NEON_H__

#define BUF_ALIGN	__attribute__ ((aligned (16)))

//#define OPTIMIZE
#define OPTIMIZE __attribute__ ((__optimize__ (4, "expensive-optimizations", "omit-frame-pointer")))


static inline uint16x8x2_t OPTIMIZE
fboptsink_convert_yuv888x16_to_rgb16x16(uint8x16_t yp, uint8x16_t up, uint8x16_t vp)
{
  int16x8_t S0 BUF_ALIGN;
  int16x8_t S22 BUF_ALIGN;
  int16x8_t S46 BUF_ALIGN;
  int16x8_t S64 BUF_ALIGN;
  int16x8_t S90 BUF_ALIGN;
  int16x8_t S114 BUF_ALIGN;
  int16x8_t S128 BUF_ALIGN;
  int16x8_t S255 BUF_ALIGN;
  uint16x8_t SxF800 BUF_ALIGN;
  uint16x8_t Sx07e0 BUF_ALIGN;
  uint16x8_t Sx001F BUF_ALIGN;

  uint8x16_t Bx00 BUF_ALIGN;
  uint8x16_t BxE0 BUF_ALIGN;
  uint8x16_t BxF8 BUF_ALIGN;

  uint8x16x2_t z BUF_ALIGN;
  uint8x16_t zl BUF_ALIGN;
  uint8x16_t zh BUF_ALIGN;

  int16x8x2_t y BUF_ALIGN;
  int16x8x2_t u BUF_ALIGN;
  int16x8x2_t v BUF_ALIGN;

  int16x8_t a0 BUF_ALIGN;
  int16x8_t a1 BUF_ALIGN;
  int16x8_t a2 BUF_ALIGN;
  int16x8_t a3 BUF_ALIGN;
  int16x8_t a4 BUF_ALIGN;

  uint16x8x2_t r BUF_ALIGN;
  uint16x8x2_t g BUF_ALIGN;
  uint16x8x2_t b BUF_ALIGN;
  int16x8_t col BUF_ALIGN;

  uint16x8_t ucol BUF_ALIGN;
  uint16x8x2_t prgb BUF_ALIGN;

  uint8x16_t prh BUF_ALIGN;
  uint8x16_t pgh BUF_ALIGN;
  uint8x16_t pgl BUF_ALIGN;
  uint8x16_t pbl BUF_ALIGN;

  // constants loading
  Bx00 = vdupq_n_u8(0x00);
  BxE0 = vdupq_n_u8(0xe0);
  BxF8 = vdupq_n_u8(0xf8);

  S0 = vdupq_n_s16(0);
  S22 = vdupq_n_s16(22);
  S46 = vdupq_n_s16(46);
  S64 = vdupq_n_s16(64);
  S90 = vdupq_n_s16(90);
  S114 = vdupq_n_s16(114);
  S128 = vdupq_n_s16(128);
  S255 = vdupq_n_s16(255);
  SxF800 = vdupq_n_u16(0xF800);
  Sx07e0 = vdupq_n_u16(0x07e0);
  Sx001F = vdupq_n_u16(0x001f);

  // conversion y, v and u (uint8x16_t -> int16x8x2_t)
  z = vzipq_u8 (yp, Bx00);
  y.val[0] = vreinterpretq_s16_u8 (z.val[0]);
  y.val[1] = vreinterpretq_s16_u8 (z.val[1]);
  z = vzipq_u8 (up, Bx00);
  u.val[0] = vreinterpretq_s16_u8 (z.val[0]);
  u.val[1] = vreinterpretq_s16_u8 (z.val[1]);
  z = vzipq_u8 (vp, Bx00);
  v.val[0] = vreinterpretq_s16_u8 (z.val[0]);
  v.val[1] = vreinterpretq_s16_u8 (z.val[1]);

  // normalize u and v
  u.val[0] = vsubq_s16(u.val[0], S128);
  u.val[1] = vsubq_s16(u.val[1], S128);
  v.val[0] = vsubq_s16(v.val[0], S128);
  v.val[1] = vsubq_s16(v.val[1], S128);

  // computes r, g, and b (low)
  a0 = vmulq_s16(S64, y.val[0]);
  a1 = vmulq_s16(S90, v.val[0]);
  a2 = vmulq_s16(S22, u.val[0]);
  a3 = vmulq_s16(S46, v.val[0]);
  a4 = vmulq_s16(S114, u.val[0]);

  col = vaddq_s16(a0, a1);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  r.val[0] = vreinterpretq_u16_s16(col);

  col = vsubq_s16(a0, a2);
  col = vsubq_s16(col, a3);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  g.val[0] = vreinterpretq_u16_s16(col);

  col = vaddq_s16(a0, a4);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  b.val[0] = vreinterpretq_u16_s16(col);

  // computes r, g, and b (high)
  a0 = vmulq_s16(S64, y.val[1]);
  a1 = vmulq_s16(S90, v.val[1]);
  a2 = vmulq_s16(S22, u.val[1]);
  a3 = vmulq_s16(S46, v.val[1]);
  a4 = vmulq_s16(S114, u.val[1]);

  col = vaddq_s16(a0, a1);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  r.val[1] = vreinterpretq_u16_s16(col);

  col = vsubq_s16(a0, a2);
  col = vsubq_s16(col, a3);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  g.val[1] = vreinterpretq_u16_s16(col);

  col = vaddq_s16(a0, a4);
  col = vshrq_n_s16(col, 6);
  col = vminq_s16(col, S255);
  col = vmaxq_s16(col, S0);
  b.val[1] = vreinterpretq_u16_s16(col);

  //pack r, g and b (low)
  ucol = vshlq_n_u16(r.val[0], 8);
  ucol = vandq_u16(ucol, SxF800);
  prgb.val[0] = ucol;

  ucol = vshlq_n_u16(g.val[0], 3);
  ucol = vandq_u16(ucol, Sx07e0);
  prgb.val[0] = vorrq_u16(prgb.val[0], ucol);

  ucol = vshrq_n_u16(b.val[0], 3);
  prgb.val[0] = vorrq_u16(prgb.val[0], ucol);

  //pack r, g and b (high)
  ucol = vshlq_n_u16(r.val[1], 8);
  ucol = vandq_u16(ucol, SxF800);
  prgb.val[1] = ucol;

  ucol = vshlq_n_u16(g.val[1], 3);
  ucol = vandq_u16(ucol, Sx07e0);
  prgb.val[1] = vorrq_u16(prgb.val[1], ucol);

  ucol = vshrq_n_u16(b.val[1], 3);
  prgb.val[1] = vorrq_u16(prgb.val[1], ucol);

  return prgb;
}

#endif
