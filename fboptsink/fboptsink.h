#ifndef __FBOPTSINK_H__
#define __FBOPTSINK_H__

#include <stdint.h>

#define FB_WIDTH 1280
#define FB_HEIGHT 720
#define FB_POSX (FB_WIDTH/2 - 640/2)
#define FB_POSY 264

#define FHD_WIDTH 1920
#define FHD_HEIGHT 1080

#define HD_WIDTH 1280
#define HD_HEIGHT 720

#define SD_WIDTH 640
#define SD_HEIGHT 360

typedef union yshape_u
{
  uint8_t fhd[FHD_HEIGHT][FHD_WIDTH];
  uint8_t hd[HD_HEIGHT][HD_WIDTH];
  uint8_t sd[SD_HEIGHT][SD_WIDTH];
} yshape_t;

typedef union uvshape_u
{
  uint8_t fhd[FHD_HEIGHT/2][FHD_WIDTH];
  uint8_t hd[HD_HEIGHT/2][HD_WIDTH];
  uint8_t sd[SD_HEIGHT/2][SD_WIDTH];
} uvshape_t;

typedef union fbshape_u
{
  uint16_t fb[FB_HEIGHT][FB_WIDTH];
} fbshape_t;

void fboptsink_blit_no_frame (fbshape_t * __restrict dst,
    yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

void fboptsink_blit_sd_frame (fbshape_t * __restrict dst,
    yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

void fboptsink_blit_hd_frame (fbshape_t * __restrict dst,
    yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

void fboptsink_blit_fhd_frame (fbshape_t * __restrict dst,
    yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

void fboptsink_blit_error_frame (fbshape_t * __restrict dst,
    yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

typedef void (*blit_func_t)(fbshape_t * __restrict dst, yshape_t * __restrict y_src, uvshape_t * __restrict uv_src);

#endif
