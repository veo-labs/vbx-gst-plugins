#include "fboptsink.h"

void
fboptsink_blit_error_frame(fbshape_t * __restrict dst, yshape_t * __restrict y_src, uvshape_t * __restrict uv_src)
{
  int i,j;

  for (i=0; i<SD_HEIGHT; i++) {
    for (j=0; j<SD_WIDTH; j++) {
      dst->fb[FB_POSY+i][FB_POSX+j] = 0xf800; // RGB565 full red
    }
  }
}
