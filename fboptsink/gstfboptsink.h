#ifndef __GST_FBOPTSINK_H__
#define __GST_FBOPTSINK_H__

#include <gst/gst.h>
#include <gst/video/gstvideosink.h>
#include <gst/video/video.h>

G_BEGIN_DECLS
#define GST_TYPE_FBOPTSINK \
  (gst_fboptsink_get_type())
#define GST_FBOPTSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_FBOPTSINK,GstFBOPTSink))
#define GST_FBOPTSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_FBOPTSINK,GstFBOPTSinkClass))
#define GST_IS_FBOPTSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_FBOPTSINK))
#define GST_IS_FBOPTSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_FBOPTSINK))
typedef struct _GstFBOPTSink GstFBOPTSink;
typedef struct _GstFBOPTSinkClass GstFBOPTSinkClass;

struct _GstFBOPTSink
{
  GstVideoSink videosink;
  int fd;
  char *fb_mem;
  gboolean fb_cleared;
  void (*blit_func)(void);
  gboolean force_nop;
  int uv_offset;
  int count;
};

struct _GstFBOPTSinkClass
{
  GstVideoSinkClass videosink_class;

};

GType gst_fboptsink_get_type (void);


G_END_DECLS
#endif /* __GST_FBOPTSINK_H__ */
