/*
 * Copyright (C) Veo-Labs http://veo-labs.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <arm_neon.h>
#include <glib.h>
#include "detect.h"

#ifdef DEBUG
#define PRINT(...)   g_print(__VA_ARGS__)
#else
#define PRINT(...)
#endif

/* Thresholds */
#define THRESHOLDS_NUM 4
static const guint8 THRESHOLDS_VALUES[THRESHOLDS_NUM] = {25, 50, 75, 100};

/* Predefined sensitivity max values */
static gdouble SENSITIVITIES_MAX_VALUES[DETECT_SENSITIVITY_LAST][THRESHOLDS_NUM] = {
  [DETECT_SENSITIVITY_HIGH] =   { 6.52, 1.30, 0.04, 0.02},
  [DETECT_SENSITIVITY_MEDIUM] = {13.04, 2.60, 0.08, 0.03},
  [DETECT_SENSITIVITY_LOW] =    {26.08, 5.20, 0.16, 0.07}
};

/* Number of loops to unroll */
#define NUM_LOOPS	4
/* Number of Y interleaved (scale factor) */
#define	INTERLEAVE	2
#define BUF_ALIGN	__attribute__ ((aligned (16)))

/* Internal type for thresholds counts calculation algorithm */
typedef gsize (*calculate_thresholds_counts_t)(detect_t *handle, guint8 *prevbuf, guint8 *curbuf, guint thresholds_counts[THRESHOLDS_NUM]);

struct detect_s {
  guint64 video_score;
  guint video_counter;
  detect_result_t prev_ret;
  detect_config_t *config;
  guint stability;
  gdouble *sensitivity_max_values;
  calculate_thresholds_counts_t calculate_thresholds_counts;
};

/* Update thresholds counts with one pixel value */
static void update_thresholds_counts(guint thresholds_counts[THRESHOLDS_NUM], guint8 pxl_diff)
{
  gint i;
  for( i=0 ; i<THRESHOLDS_NUM ; i++ ) {
    if( pxl_diff > THRESHOLDS_VALUES[i] ) {
      thresholds_counts[i]++;
    }
  }
}
/* Calculate thresholds counts for YUY2 format */
static gsize calculate_thresholds_counts_yuy2(detect_t *handle, guint8 *prevbuf, guint8 *curbuf, guint thresholds_counts[THRESHOLDS_NUM])
{
  gsize size = handle->config->size * 1 / 2; /* Only calculate thresholds on Y values */
  gsize count = size;
  guint8 pxl_diff;
  guint8 *prevbuf_pointer = prevbuf;
  guint8 *curbuf_pointer = curbuf;

  while( count > 0 ) {
    pxl_diff = ABS(*prevbuf_pointer - *curbuf_pointer);
    update_thresholds_counts(thresholds_counts, pxl_diff);
    count--;
    prevbuf_pointer += 2;
    curbuf_pointer += 2;
  }

  return size;
}


#ifdef DEBUG

static void display_vector(int size, uint8_t * vect)
{
	int i;

	for (i=0; i < size; i++) {
		g_print("%02d ", i);
	}
	g_print("\n");

	for (i=0; i < size; i++) {
		g_print("---", vect[i]);
	}
	g_print("\n");

	for (i=0; i < size; i++) {
		g_print("%02x ", vect[i]);
	}
	g_print("\n");

	g_print("\n");
}

guint8 * create_fake_buffer(int n, int start, int mod, int inc)
{
	int i;

	guint8 * buffer = g_malloc(n);

	for (i=0; i < n; i++) {
		buffer[i] = start;
		start += inc;
		start = start % mod;
	}

	return buffer;
}

#endif /* DEBUG */


#define VECTOR_SIZE 16
#define ACC_MAX_VALUE 255
#define SKIP_CHUNK_MOD 8

static gsize calculate_thresholds_counts_yuy2_neon(detect_t *handle, guint8 *prevbuf_p, guint8 *curbuf_p, guint result[THRESHOLDS_NUM])
{

	uint8x16_t threshold[THRESHOLDS_NUM] BUF_ALIGN;
	uint8x16_t acc[THRESHOLDS_NUM] BUF_ALIGN;
	uint8x16x4_t prev4 BUF_ALIGN;
	uint8x16x4_t cur4 BUF_ALIGN;
	uint8x16_t prev BUF_ALIGN;
	uint8x16_t cur BUF_ALIGN;
	uint8x16_t diff BUF_ALIGN;
	uint8x16_t mask BUF_ALIGN;
	uint8x16_t comp BUF_ALIGN;
	uint8x16_t masked_comp BUF_ALIGN;
	uint8_t transfer[VECTOR_SIZE];
	int i, j, k;
	guint8 *prevbuf;
	guint8 *curbuf;
	int num_y, num_proceed_y;
	gboolean done;
	int skip_chunk;

#ifdef DEBUG_FAKEBUF
	prevbuf_p = create_fake_buffer(handle->config->size, 60, 0x100, 0);
	curbuf_p = create_fake_buffer(handle->config->size, 120, 0x100, 0);
#endif

	for (i = 0 ; i < THRESHOLDS_NUM ; i++) {
		threshold[i] = vdupq_n_u8(THRESHOLDS_VALUES[i]);
	}
	mask = vdupq_n_u8(0x01);

	prevbuf = prevbuf_p;
	curbuf = curbuf_p;

	for (i = 0 ; i < THRESHOLDS_NUM ; i++) {
		result[i] = 0;
	}

	num_proceed_y = 0;
	num_y = handle->config->width * handle->config->height;
	done = FALSE;
	skip_chunk = 0;

	while(!done) {
		for (i = 0 ; i < THRESHOLDS_NUM ; i++) {
			acc[i] = vdupq_n_u8(0x00);
		}

		for (j=0; j < ACC_MAX_VALUE; j++) {
			if ((num_proceed_y + VECTOR_SIZE*2) > num_y) {
				done = TRUE;
				break;
			}

			if (skip_chunk==0) {
				prev4 = vld4q_u8(prevbuf);
				prev = prev4.val[0];
				cur4 = vld4q_u8(curbuf);
				cur = cur4.val[0];

				diff = vabdq_u8(cur, prev);

				for (i=0; i < THRESHOLDS_NUM; i++) {
					comp = vcgtq_u8(diff, threshold[i]);
					masked_comp = vandq_u8(comp, mask);
					acc[i] = vaddq_u8(acc[i], masked_comp);
				}
			}

			skip_chunk = (skip_chunk+1) % SKIP_CHUNK_MOD;

			num_proceed_y += VECTOR_SIZE * 2;
			prevbuf += VECTOR_SIZE * 4;
			curbuf += VECTOR_SIZE * 4;
		}

		for (i=0; i < THRESHOLDS_NUM; i++) {
			vst1q_u8(transfer, acc[i]);
			for (j=0; j < VECTOR_SIZE; j++) {
				result[i] += transfer[j] * 2 * SKIP_CHUNK_MOD;
			}
		}
	}

#ifdef DEBUG_FAKEBUF
	g_free(prevbuf_p);
	g_free(curbuf_p);
#endif

	return num_proceed_y;
}


/* Calculate thresholds counts for NV12 format */
static gsize calculate_thresholds_counts_nv12(detect_t *handle, guint8 *__restrict__ prevbuf, guint8 *__restrict__ curbuf, guint thresholds_counts[THRESHOLDS_NUM])
{
  gsize size = handle->config->size *2/3; /* Only calculate thresholds on Y values */
  gsize count = size;
  guint8 pxl_diff;
  guint8 *prevbuf_pointer = prevbuf;
  guint8 *curbuf_pointer = curbuf;

  while( count > 0 ) {
    pxl_diff = ABS(*prevbuf_pointer - *curbuf_pointer);
    update_thresholds_counts(thresholds_counts, pxl_diff);
    count--;
    prevbuf_pointer++;
    curbuf_pointer++;
  }

  return size;
}

/* Calculate thresholds counts for NV12 format with NEON optimizations */
static gsize calculate_thresholds_counts_nv12_neon(detect_t *handle, guint8 *__restrict__ prevbuf, guint8 *__restrict__ curbuf, guint thresholds_counts[THRESHOLDS_NUM])
{
  gsize size = handle->config->size *2/3; /* Only calculate thresholds on Y values */
  gsize count = size;
	gsize width = handle->config->width;
	/* WARN: INTERLEAVE can change, but you need to change vld manually */
	/* INTERLEAVE=1 => vld1 => uint8x16_t */
	/* INTERLEAVE=2 => vld2 => uint8x16x2_t */
	uint8x16x2_t vec_prev;
	uint8x16x2_t vec_cur;
	uint8x16_t vec_absdiff;
	uint8x16_t threshold[THRESHOLDS_NUM];
	guint8 j, n, inter;
	guint pix_per_loop = width/(16*INTERLEAVE*NUM_LOOPS);
	guint w_count;
	uint64_t local_thresh[THRESHOLDS_NUM];
	uint64_t local_thresh_ones[THRESHOLDS_NUM];
	uint16x8_t and_vector = vdupq_n_u16(0x8001);

	/* Load 16 times the current threshold */
	for (n = 0 ; n < THRESHOLDS_NUM ; n++) {
		threshold[n] = vdupq_n_u8(THRESHOLDS_VALUES[n]);
		thresholds_counts[n] = 0;
	}

	while (count > 0) {
		w_count = pix_per_loop;
		for (n = 0 ; n < THRESHOLDS_NUM ; n++) {
			local_thresh[n] = 0;
			local_thresh_ones[n] = 0;
		}
		while (w_count > 0) {
			for (j = 0 ; j < NUM_LOOPS ; j++) {
				/* Load 16 Y of previous buffer while scaling by INTERLEAVE step on width */
				/* WARN: INTERLEAVE can change, but you need to change vld manually */
				vec_prev = vld2q_u8(prevbuf);
				prevbuf += 16*INTERLEAVE;
				/* Load 16 Y of current buffer */
				vec_cur = vld2q_u8(curbuf);
				curbuf += 16*INTERLEAVE;
				/* abs ( cur - prev ) of 16 Y */
				for (inter = 0 ; inter < INTERLEAVE ; inter++) {
					vec_absdiff = vabdq_u8(vec_prev.val[inter], vec_cur.val[inter]);
					/* Compare diff to first row of values */
					for (n = 0 ; n < THRESHOLDS_NUM ; n+=2) {
						/* compare 16 values with threshold */
						uint8x16_t comp1 = vcgtq_u8(vec_absdiff, threshold[n]);
						/* AND values to get only 1 or 0 in each lane */
						uint16x8_t comp1_anded = vandq_u16((uint16x8_t)comp1, and_vector);
						/* compare 16 values with threshold */
						uint8x16_t comp2 = vcgtq_u8(vec_absdiff, threshold[n+1]);
						/* Count number of leading zeroes */
						uint16x8_t vclz = vclzq_u16(comp1_anded);
						/* AND values to get only 1 or 0 in each lane */
						uint16x8_t comp2_anded = vandq_u16((uint16x8_t)comp2, and_vector);
						/* Count number of leading zeroes */
						uint16x8_t vclz2 = vclzq_u16(comp2_anded);

						/* Two Q with zipped values in first 64 bits for both threshold elements */
						uint8x16x2_t vclz_unzip = vuzpq_u8((uint8x16_t)vclz, (uint8x16_t)vclz2);
						/* Pairwise add 16 values on 8 bits for first threshold*/
						uint16x8_t add1_th0 = vpaddlq_u8(vclz_unzip.val[0]);
						/* Pairwise add 16 values on 8 bits for second threshold */
						uint16x8_t add1_th1 = vpaddlq_u8(vclz_unzip.val[1]);
						/* Pairwise add 8 values on 16 bits for first threshold*/
						uint32x4_t add2_th0 = vpaddlq_u16(add1_th0);
						/* Pairwise add 8 values on 16 bits for second threshold*/
						uint32x4_t add2_th1 = vpaddlq_u16(add1_th1);
						/* Pairwise add 4 values on 32 bits for first threshold*/
						uint64x2_t add3_th0 = vpaddlq_u32(add2_th0);
						/* Pairwise add 4 values on 32 bits for second threshold*/
						uint64x2_t add3_th1 = vpaddlq_u32(add2_th1);

						/* Get number of zeroes of the 16 values tested in first 64 bits vector
						 * against the first threshold */
						local_thresh[n] = vgetq_lane_u64(add3_th0, 0);
						local_thresh_ones[n] += 128 - local_thresh[n];
//						PRINT("[%d, %u] ", n, local_thresh_ones[n]);
						/* Get number of zeroes of the 16 values tested in first 64 bits vector
						 * against the second threshold */
						local_thresh[n+1] = vgetq_lane_u64(add3_th1, 0);
						local_thresh_ones[n+1] += 128 - local_thresh[n+1];
//						PRINT("[%d, %u] ", n+1, local_thresh_ones[n+1]);
					}
//					PRINT("\n");
				}
			}
			w_count--;
		}
		/* Scale on Height */
		prevbuf += (INTERLEAVE-1)*width;
		curbuf += (INTERLEAVE-1)*width;
		/* Store the number of ones counted (ie. 128 - number of zeroes) for each threshold */
		for (n = 0 ; n < THRESHOLDS_NUM ; n++) {
			thresholds_counts[n] += local_thresh_ones[n];
		}
		count -= INTERLEAVE*width;
	}

  return size;
}
/*
 *  1) For each byte in both prevbuf and curbuf, calculate: diff_value = ABS(*prevbuf - *curbuf)
 *  2) Compare diff_value to thresholds and increment counter for each threshold if the diff_value is superior.
 *  3) Then after the whole buffer is walked through, compare thresholds counters to sensitivity max values.
 *     If one counter is superior to the corresponding sensitivity max value, then the buffer is considered
 *     to have significantly changed.
 */
static gboolean buffer_has_changed(detect_t *handle, guint8 *prevbuf, guint8 *curbuf)
{
  gint i;
  gsize size;
  gdouble normalized;
  guint thresholds_counts[THRESHOLDS_NUM] = {0};
	gboolean ret;

  size = handle->calculate_thresholds_counts(handle, prevbuf, curbuf, thresholds_counts);

	ret = FALSE;

  /* PRINT("Raw thresholds counts:"); */
  /* for(i=0 ; i<THRESHOLDS_NUM ; i++) { */
  /*   PRINT(" %d", thresholds_counts[i]); */
  /* } */
	/* PRINT("\n"); */

  PRINT("Normalized thresholds counts:");
  for(i=0 ; i<THRESHOLDS_NUM ; i++) {
    normalized = (thresholds_counts[i] / (gdouble)size) * 100.0;
		if( normalized > handle->sensitivity_max_values[i] )
			ret = TRUE;
    PRINT(" %.02f", normalized);
  }

	if (ret) {
		PRINT(" >> DETECT <<");
	}

	PRINT("\n");

  return ret;
}

/* Return sensitivity values from sensitivity setting */
static gdouble *get_sensitivity_max_values(detect_sensitivity_t sensitivity)
{
  if( sensitivity < 0 || sensitivity >= sizeof(SENSITIVITIES_MAX_VALUES) ) {
    return NULL;
  }

  return SENSITIVITIES_MAX_VALUES[sensitivity];
}

/* Return sensitivity values from sensitivity setting */
static calculate_thresholds_counts_t get_calculate_thresholds_counts_algorithm(detect_config_t *config)
{
	gboolean neon_optimization = FALSE;
	if (((config->width) % (16*INTERLEAVE*NUM_LOOPS)) == 0) {
		neon_optimization = TRUE;
		PRINT("Optimized function will be called\n");
	}

  switch(config->format) {
    case DETECT_FORMAT_NV12:
			if (neon_optimization == TRUE)
				return &calculate_thresholds_counts_nv12_neon;
			else
				return &calculate_thresholds_counts_nv12;
    case DETECT_FORMAT_YUY2:
				return &calculate_thresholds_counts_yuy2_neon;
    default: break;
  }
  return NULL;
}

/*
 * we currently assume for this function a framerate of 5/1
 *
 * h->stability: increased when buffer is stable, reset if buffer change
 * video_score: score between 0 (not video) and 64 (video) which consider last 64 calls to this function
 * video_counter: frame counter which is used to compute DETECT_VIDEO
 *
 */
detect_result_t detect_change(detect_t *h, guint8 *previous_buffer, guint8 *current_buffer)
{
  gboolean buffer_changed = FALSE;
  gint video_score;
  gint video_counter;
  detect_result_t ret;

  if (previous_buffer == NULL) {
    h->stability = 5; // prevent double detection of first frame
    return DETECT_FIRST_FRAME;
  }
  else if(buffer_has_changed(h, previous_buffer, current_buffer)) {
    buffer_changed = TRUE;
  }
  h->video_score = (h->video_score << 1);
  h->video_counter++;
  if (buffer_changed) {
    h->stability = 0;
    h->video_score |= 0x01;
  }
  else {
    h->stability ++;
  }
  video_score = __builtin_popcountll(h->video_score);
  //g_print("stability:%d video_score:%d video_counter:%d\n", h->stability, video_score, h->video_counter);
  ret = DETECT_NOTHING;
  // detect stable image
  if ((h->stability == 2)  &&  (video_score < 5)) {
    ret = DETECT_STABLE_IMAGE;
    h->video_counter = 0;
  }
  // detect video
  else if ((video_score > 30)  &&  (h->video_counter >= 150)) {
    ret = DETECT_VIDEO;
    h->video_counter = 0;
  }
  // detect stable image after video
  else if ((h->prev_ret == DETECT_VIDEO)  &&  (video_score < 10)) {
    ret = DETECT_STABLE_IMAGE;
    h->video_counter = 0;
  }
  // detect video after video
  else if ((h->prev_ret == DETECT_VIDEO)  &&  (h->video_counter >= 150)) {
    ret = DETECT_VIDEO;
    h->video_counter = 0;
  }
  if (ret != DETECT_NOTHING) {
    h->prev_ret = ret;
  }
  return ret;
}


detect_t *detect_init(detect_config_t *config)
{
  detect_t *handle;

  handle = g_new(detect_t, 1);

  handle->config = config;
  handle->video_counter = 0;
  handle->video_score = 0;
  handle->stability = 0;
  handle->prev_ret = DETECT_NOTHING;

  handle->calculate_thresholds_counts = get_calculate_thresholds_counts_algorithm(config);
  if( handle->calculate_thresholds_counts == NULL ) {
    detect_cleanup(handle);
    return NULL;
  }

  handle->sensitivity_max_values = get_sensitivity_max_values(DETECT_SENSITIVITY_MEDIUM);
  if( handle->sensitivity_max_values == NULL ) {
    detect_cleanup(handle);
    return NULL;
  }

  return handle;
}

void detect_cleanup(detect_t *handle)
{
  g_free(handle);
}
