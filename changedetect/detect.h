/*
 * Copyright (C) 2013 Veo-Labs http://veo-labs.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _DETECT_H
#define _DETECT_H

#include <glib.h>

/* Return values of detect method */
typedef enum detect_result_e {
  DETECT_NOTHING,
  DETECT_FIRST_FRAME,
  DETECT_STABLE_IMAGE,
  DETECT_VIDEO
} detect_result_t;

/* Levels of sensitivity */
typedef enum detect_sensitivity_e {
  DETECT_SENSITIVITY_LOW,
  DETECT_SENSITIVITY_MEDIUM,
  DETECT_SENSITIVITY_HIGH,
  DETECT_SENSITIVITY_LAST
} detect_sensitivity_t;

/* Accepted buffer formats */
typedef enum detect_format_e {
  DETECT_FORMAT_NV12,
  DETECT_FORMAT_YUY2
} detect_format_t;

/* Configuration */
typedef struct detect_config_s {
  detect_format_t format;
  gsize size;
  gsize width;
	gsize height;
} detect_config_t;

/* State of the module */
typedef struct detect_s detect_t;

/* Initialize detection and return handle */
detect_t *detect_init(detect_config_t *config);

/* Free handle */
void detect_cleanup(detect_t *handle);

/*
 * Detect change in a buffer.
 * Return a detect_result_t value
 */
detect_result_t detect_change(detect_t *handle, guint8 *previous_buffer, guint8 *current_buffer);

#endif
