/*
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-changedetect
 *
 * Detect changes in a video stream
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -e videotestsrc pattern=blink num-buffers=10 is-live=true ! \
 *   video/x-raw,width=1280,height=720,framerate=1/2 ! \
 *   videorate ! video/x-raw,framerate=30/1 ! \
 *   queue ! videoscale ! video/x-raw,width=160,height=90 ! \
 *   changedetect name=d ! queue ! jpegenc ! multifilesink location=slide%d.jpeg
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasetransform.h>
#include <gst/video/video.h>

#include "gstchangedetect.h"

GST_DEBUG_CATEGORY_STATIC (gst_change_detect_debug);
#define GST_CAT_DEFAULT gst_change_detect_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

/* Caps for the detection pad */
#define CAPS \
  "video/x-raw" \
  ", format=(string) { YUY2, NV12 }" \
  ", width=(int) [ 1, 4096 ]" \
  ", height=(int) [ 1, 4096 ]"

/* the capabilities of the inputs and outputs.
 */
static GstStaticPadTemplate sink_template =
GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (CAPS)
);

static GstStaticPadTemplate src_template =
GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (CAPS)
);

#define gst_change_detect_parent_class parent_class
G_DEFINE_TYPE (GstChangeDetect, gst_change_detect, GST_TYPE_BASE_TRANSFORM);

static void gst_change_detect_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_change_detect_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstFlowReturn gst_change_detect_transform_ip (GstBaseTransform * base,
    GstBuffer * outbuf);
static gboolean gst_change_detect_setcaps (GstBaseTransform * trans,
    GstCaps * in_caps, GstCaps * out_caps);
static gboolean gst_change_detect_start (GstBaseTransform * trans);
static gboolean gst_change_detect_stop (GstBaseTransform * trans);

/* Sensitivity type */
#define GST_TYPE_CHANGE_DETECT_SENSITIVITY (gst_change_detect_sensitivity_get_type ())
static GType
gst_change_detect_sensitivity_get_type (void)
{
  static GType sensitivity_type = 0;
  static const GEnumValue sensitivity_types[] = {
    {GST_CHANGE_DETECT_SENSITIVITY_HIGH, "High sensitivity", "high"},
    {GST_CHANGE_DETECT_SENSITIVITY_MEDIUM, "Medium sensitivity", "medium"},
    {GST_CHANGE_DETECT_SENSITIVITY_LOW, "Low sensitivity", "low"},
    {0, NULL, NULL}
  };

  if (!sensitivity_type) {
    sensitivity_type =
        g_enum_register_static ("GstChangeDetectSensitivity", sensitivity_types);
  }
  return sensitivity_type;
}

/* GObject vmethod implementations */

/* initialize the changedetect's class */
static void
gst_change_detect_class_init (GstChangeDetectClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  GstBaseTransformClass *base_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class->set_property = gst_change_detect_set_property;
  gobject_class->get_property = gst_change_detect_get_property;

  gst_element_class_set_details_simple(gstelement_class,
    "ChangeDetect",
    "Generic/Filter",
    "Only transfer buffers that have significantly changed",
    "Veo-Labs http://www.veo-labs.com/");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_template));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_template));

  base_class->transform_ip = GST_DEBUG_FUNCPTR (gst_change_detect_transform_ip);
  base_class->set_caps = GST_DEBUG_FUNCPTR (gst_change_detect_setcaps);
  base_class->start = GST_DEBUG_FUNCPTR (gst_change_detect_start);
  base_class->stop = GST_DEBUG_FUNCPTR (gst_change_detect_stop);

  GST_DEBUG_CATEGORY_INIT (gst_change_detect_debug, "changedetect", 0, "Changedetect filter");
}

/* initialize the new element
 * initialize instance structure
 */
static void
gst_change_detect_init (GstChangeDetect *filter)
{
  gst_base_transform_set_passthrough(GST_BASE_TRANSFORM (filter), TRUE);

  /* Initialize instance structure */
  filter->detect = NULL;

  /* Image dimensions will be updated during caps negociation */
  filter->prevbuf = NULL;
}

static void
gst_change_detect_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstChangeDetect *filter = GST_CHANGE_DETECT (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_change_detect_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstChangeDetect *filter = GST_CHANGE_DETECT (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* this function does the actual processing
 */
static GstFlowReturn
gst_change_detect_transform_ip (GstBaseTransform * base, GstBuffer * curbuf)
{
  GstChangeDetect *filter = GST_CHANGE_DETECT (base);
  detect_result_t detect_result;
  GstMapInfo curbuf_map_info, prevbuf_map_info;
  GstFlowReturn  ret = GST_BASE_TRANSFORM_FLOW_DROPPED;
  guint8 *prevbuf_data = NULL;
  GstClock *clock;
  GstClockTime ts1, ts2;
  GstClockTimeDiff tsdiff;

  GST_LOG_OBJECT (filter, "Entering transform_ip with curbuf: %"
  GST_PTR_FORMAT " prevbuf: %" GST_PTR_FORMAT, curbuf, filter->prevbuf);

  if( gst_buffer_map(curbuf, &curbuf_map_info, GST_MAP_READ) == FALSE ) {
    GST_ERROR_OBJECT(filter, "Failed to read current buffer data");
    return GST_FLOW_ERROR;
  }
  if (filter->prevbuf != NULL) {
    if( gst_buffer_map(filter->prevbuf, &prevbuf_map_info, GST_MAP_READ) == FALSE ) {
      GST_ERROR_OBJECT(filter, "Failed to read previous buffer data");
      return GST_FLOW_ERROR;
    } else {
      prevbuf_data = prevbuf_map_info.data;
    }
  }

  clock = gst_system_clock_obtain ();
  ts1 = gst_clock_get_time (clock);
  detect_result = detect_change (filter->detect, prevbuf_data, curbuf_map_info.data);
  ts2 = gst_clock_get_time (clock);
  tsdiff = GST_CLOCK_DIFF(ts1, ts2);
  GST_DEBUG_OBJECT (filter, "Time spent in detect_change : %lld", tsdiff);
  gst_object_unref(clock);
  switch (detect_result)
  {
    case DETECT_FIRST_FRAME:
      ret = GST_FLOW_OK;
      GST_INFO_OBJECT (filter, "First frame detected");
      //g_print("=== First frame detected ===\n");
      break;
    case DETECT_STABLE_IMAGE:
      ret = GST_FLOW_OK;
      GST_INFO_OBJECT (filter, "Stable image detected");
      //g_print("=== Stable image detected ===\n");
      break;
    case DETECT_VIDEO:
      ret = GST_FLOW_OK;
      GST_INFO_OBJECT (filter, "Video detected");
      //g_print("=== Video detected ===\n");
      break;
    default:
    case DETECT_NOTHING:
      GST_DEBUG_OBJECT (filter, "detect nothing, dropping buffer");
      ret = GST_BASE_TRANSFORM_FLOW_DROPPED;
  }

  gst_buffer_unmap(curbuf, &curbuf_map_info);
  if (filter->prevbuf != NULL) {
    gst_buffer_unmap(filter->prevbuf, &prevbuf_map_info);
    gst_buffer_unref(filter->prevbuf);
  }
  filter->prevbuf = gst_buffer_ref(curbuf);

  return ret;
}

/* Caps set for detect sink pad */
static gboolean
gst_change_detect_sinkpad_set_caps (GstBaseTransform * trans, GstCaps * caps)
{
  gint width, height;
  const gchar *format;
  const GstStructure *cs;
  GstChangeDetect *filter = GST_CHANGE_DETECT(trans);

  g_return_val_if_fail(gst_caps_is_fixed(caps), FALSE);

  /* Get format from caps */
  cs = gst_caps_get_structure(caps, 0);
  format = gst_structure_get_string(cs, "format");
  if( format == NULL ) {
     GST_ERROR_OBJECT(filter, "No image format available in caps");
     return FALSE;
  }

  /* Get width and height from caps */
  if( gst_structure_get_int(cs, "width", &width) == FALSE ||
      gst_structure_get_int(cs, "height", &height) == FALSE ) {
    GST_ERROR_OBJECT(filter, "No width/height available in caps");
    return FALSE;
  }

	filter->detect_config.width = width;
	filter->detect_config.height = height;
  GST_INFO_OBJECT(filter, "Detection video size is %dx%d", width, height);

  /* Configure detection module based on caps */
  if( g_strcmp0(format, "NV12") == 0 ) {
    filter->detect_config.size = width*height*3/2;
    filter->detect_config.format = DETECT_FORMAT_NV12;
  }
  else if( g_strcmp0(format, "YUY2") == 0 ) {
    filter->detect_config.size = width*height*2;
    filter->detect_config.format = DETECT_FORMAT_YUY2;
  }
  else {
    GST_ERROR_OBJECT(filter, "Invalid format negotiated");
    return FALSE;
  }
  filter->detect = detect_init(&filter->detect_config);
  if (filter->detect == NULL)
    return FALSE;
  return TRUE;
}
static gboolean
gst_change_detect_setcaps (GstBaseTransform * trans, GstCaps * in_caps,
    GstCaps * out_caps)
{

  GST_DEBUG_OBJECT (trans, "setcaps called in: %" GST_PTR_FORMAT
      " out: %" GST_PTR_FORMAT, in_caps, out_caps);
  return gst_change_detect_sinkpad_set_caps(trans, in_caps);

}

static gboolean gst_change_detect_start (GstBaseTransform * trans)
{
	gboolean ret = TRUE;

  return ret;
}

static gboolean gst_change_detect_stop (GstBaseTransform * trans)
{
  GstChangeDetect *filter = GST_CHANGE_DETECT(trans);
  if (filter->prevbuf)
    gst_buffer_unref(filter->prevbuf);
  filter->prevbuf = NULL;

  detect_cleanup(filter->detect);
  return TRUE;
}

/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
change_detect_init (GstPlugin * changedetect)
{

  return gst_element_register (changedetect, "changedetect", GST_RANK_NONE,
      GST_TYPE_CHANGE_DETECT);
}


#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
  #define VERSION "2.0"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    changedetect,
    "Detect changes in video stream",
    change_detect_init,
    VERSION,
    "LGPL",
    "Veo-Labs",
    "http://www.veo-labs.com/"
)
