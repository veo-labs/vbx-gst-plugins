/* Conditional selector plugin
 * Copyright (C) Veo-Labs http://www.veo-labs.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-conditionalselector
 *
 * Conditional selector is a input-selector that can change dinamically
 * from one sink to another depending on some condition, usually a buffer flag
 * condition.
 * Sinks format should be equals.
 * 
 * Individual parameters for each input stream can be configured on the
 * #GstCompositorPad:
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>

#include "gstconditionalselector.h"

GST_DEBUG_CATEGORY_STATIC (conditional_selector_debug);
#define GST_CAT_DEFAULT conditional_selector_debug

#define gst_conditional_selector_parent_class parent_class
G_DEFINE_TYPE (GstConditionalSelector, gst_conditional_selector,
    GST_TYPE_ELEMENT);

/* Function definition */
static GstFlowReturn gst_conditional_selector_sink_chain (GstPad * pad,
    GstObject * parent, GstBuffer * buf);
static gboolean gst_conditional_selector_sink_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static gboolean gst_conditional_selector_sink_query (GstPad * pad,
    GstObject * parent, GstQuery * query);
static gboolean gst_conditional_selector_src_event (GstPad * pad,
    GstObject * parent, GstEvent * event);

/* Pad template definition */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate main_sink_factory =
GST_STATIC_PAD_TEMPLATE ("main_sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

static GstStaticPadTemplate aux_sink_factory =
GST_STATIC_PAD_TEMPLATE ("aux_sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

/* Lock and condition function definition */
#define GST_CONDITIONAL_SELECTOR_GET_COND(csel) (&((GstConditionalSelector*)(csel))->cond)
#define GST_CONDITIONAL_SELECTOR_GET_LOCK(csel) (&((GstConditionalSelector*)(csel))->lock)
#define GST_CONDITIONAL_SELECTOR_LOCK(csel) (g_mutex_lock (GST_CONDITIONAL_SELECTOR_GET_LOCK(csel)))
#define GST_CONDITIONAL_SELECTOR_UNLOCK(csel) (g_mutex_unlock (GST_CONDITIONAL_SELECTOR_GET_LOCK(csel)))
#define GST_CONDITIONAL_SELECTOR_WAIT(csel) (g_cond_wait (GST_CONDITIONAL_SELECTOR_GET_COND(csel), \
		GST_CONDITIONAL_SELECTOR_GET_LOCK(csel)))
#define GST_CONDITIONAL_SELECTOR_BROADCAST(csel) (g_cond_broadcast (GST_CONDITIONAL_SELECTOR_GET_COND(csel)))



/* must be called without the CONDITIONAL_SELECTOR_LOCK locked, will wait until
 * the running time of the active pad is after this pad or return TRUE when flushing */
static gboolean
gst_conditional_selector_wait_running_time (GstConditionalSelector * csel,
    GstPad * pad, GstBuffer * buf)
{
  GST_DEBUG_OBJECT (csel, "entering wait for buffer %p", buf);

  /* If we have no valid timestamp we can't sync this buffer */
  if (!GST_BUFFER_PTS_IS_VALID (buf)) {
    GST_DEBUG_OBJECT (csel, "leaving wait for buffer with "
        "invalid timestamp");
    return FALSE;
  }

  /* Wait until
   *   a) the buffer running time is before the main sink running time
   *   b) the pad or the selector is flushing
   */
  GST_CONDITIONAL_SELECTOR_LOCK (csel);
  while (TRUE) {

    /* the buffer running time is before the main sink running time */
    if (csel->main_timestamp > GST_BUFFER_PTS (buf)) {
      GST_CONDITIONAL_SELECTOR_UNLOCK (csel);
      return FALSE;
    }

    /* Flush event has arrived to any pad */
    if (csel->flushing || csel->eos) {
      GST_CONDITIONAL_SELECTOR_UNLOCK (csel);
      return TRUE;
    }

    GST_DEBUG_OBJECT (csel, "Waiting for changes");
    /* Wait to main sink to advance */
    GST_CONDITIONAL_SELECTOR_WAIT (csel);
  }
}

/*
 * Enter with GST_CONDITIONAL_SELECTOR_LOCK locked.
 * Default check condition function which check if the value of the
 * buffer flags are equals to csel->condition.
 */
static gboolean
gst_conditional_selector_check_condition (GstObject * parent, GstPad * pad,
    GstBuffer * buf)
{
  GstConditionalSelector *csel = GST_CONDITIONAL_SELECTOR (parent);

  return GST_BUFFER_FLAGS (buf) == csel->condition;
}

static GstFlowReturn
gst_conditional_selector_sink_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buf)
{
  GstConditionalSelector *csel = GST_CONDITIONAL_SELECTOR (parent);
  GstConditionalSelectorClass *klass =
      GST_CONDITIONAL_SELECTOR_GET_CLASS (csel);
  GstFlowReturn res = GST_FLOW_OK;

  GST_CONDITIONAL_SELECTOR_LOCK (csel);

  if (pad == csel->main_pad) {

    csel->condition_active = klass->check_condition (parent, pad, buf);
    csel->main_timestamp = GST_BUFFER_PTS (buf);

    if (csel->condition_active) {
      GST_LOG_OBJECT (csel, "Discard corrupted buffer: %" GST_PTR_FORMAT, buf);
      gst_buffer_unref (buf);
    } else {
      GST_LOG_OBJECT (csel, "Using main buffer: %" GST_PTR_FORMAT, buf);
      res = gst_pad_push (csel->src_pad, buf);
    }

    /* Aux pad could be waiting for */
    GST_CONDITIONAL_SELECTOR_BROADCAST (csel);

  } else if (csel->condition_active) {

    GST_CONDITIONAL_SELECTOR_UNLOCK (csel);
    /* Synchronize streams to main running stream */
    if (gst_conditional_selector_wait_running_time (csel, csel->aux_pad, buf))
      goto flushing;
    GST_CONDITIONAL_SELECTOR_LOCK (csel);

    /*
     * FIXME: Take the timestamp of main sink, what happend if main sink stop
     * sending buffers? Repeated timestamps
     */
    GST_BUFFER_PTS (buf) = csel->main_timestamp;
    GST_LOG_OBJECT (csel, "Using aux buffer: %" GST_PTR_FORMAT, buf);
    res = gst_pad_push (csel->src_pad, buf);

  } else {
    GST_LOG_OBJECT (csel, "Discard aux buffer: %" GST_PTR_FORMAT, buf);
    GST_CONDITIONAL_SELECTOR_UNLOCK (csel);
    if (gst_conditional_selector_wait_running_time (csel, csel->aux_pad, buf))
      goto flushing;

    GST_CONDITIONAL_SELECTOR_LOCK (csel);
    gst_buffer_unref (buf);
    res = GST_FLOW_OK;
  }

  GST_CONDITIONAL_SELECTOR_UNLOCK (csel);

done:
  return res;

flushing:
  {
    GST_DEBUG_OBJECT (csel, "We are flushing, discard buffer %p", buf);
    gst_buffer_unref (buf);
    res = GST_FLOW_FLUSHING;
    goto done;
  }
}

static gboolean
gst_conditional_selector_sink_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  GstConditionalSelector *csel = GST_CONDITIONAL_SELECTOR (parent);
  gboolean res = TRUE;

  GST_DEBUG_OBJECT (csel, "%" GST_PTR_FORMAT " received query %" GST_PTR_FORMAT,
      pad, event);

  GST_CONDITIONAL_SELECTOR_LOCK (csel);

  /* Control flush/eos event */
  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_FLUSH_START:
      /* Unblock the pad if it's waiting */
      csel->flushing = TRUE;
      break;
    case GST_EVENT_FLUSH_STOP:
      csel->flushing = FALSE;
      break;
    case GST_EVENT_EOS:
      /* Aux pad could be waiting for */
      csel->eos = TRUE;
      GST_CONDITIONAL_SELECTOR_BROADCAST (csel);
      break;
    default:
      break;
  }

  GST_CONDITIONAL_SELECTOR_UNLOCK (csel);

  if (pad == csel->main_pad)
    res = gst_pad_push_event (csel->src_pad, event);
  else
    res = gst_pad_event_default (pad, parent, event);

  return res;
}

static gboolean
gst_conditional_selector_sink_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  GstConditionalSelector *csel = GST_CONDITIONAL_SELECTOR (parent);
  gboolean res = FALSE;

  GST_DEBUG_OBJECT (csel, "%" GST_PTR_FORMAT " received query %" GST_PTR_FORMAT,
      pad, query);

  if (pad == csel->main_pad)
    res = gst_pad_peer_query (csel->src_pad, query);
  else
    res = gst_pad_query_default (pad, parent, query);

  return res;
}

static gboolean
gst_conditional_selector_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  GstConditionalSelector *csel = GST_CONDITIONAL_SELECTOR (parent);
  gboolean res = FALSE;
  GstIterator *iter;
  gboolean done = FALSE;
  GValue item = { 0, };
  GstPad *eventpad;
  GList *pushed_pads = NULL;

  GST_DEBUG_OBJECT (csel, "received event %" GST_PTR_FORMAT, event);

  /* Send upstream events to all sinkpads */
  iter = gst_element_iterate_sink_pads (GST_ELEMENT_CAST (csel));

  /* This is now essentially a copy of gst_pad_event_default_dispatch
   * with a different iterator */
  while (!done) {
    switch (gst_iterator_next (iter, &item)) {
      case GST_ITERATOR_OK:
        eventpad = g_value_get_object (&item);

        /* if already pushed,  skip */
        if (g_list_find (pushed_pads, eventpad)) {
          g_value_reset (&item);
          break;
        }

        gst_event_ref (event);
        res |= gst_pad_push_event (eventpad, event);

        g_value_reset (&item);
        break;
      case GST_ITERATOR_RESYNC:
        /* We don't reset the result here because we don't push the event
         * again on pads that got the event already and because we need
         * to consider the result of the previous pushes */
        gst_iterator_resync (iter);
        break;
      case GST_ITERATOR_ERROR:
        GST_ERROR_OBJECT (pad, "Could not iterate over sinkpads");
        done = TRUE;
        break;
      case GST_ITERATOR_DONE:
        done = TRUE;
        break;
    }
  }
  g_value_unset (&item);
  gst_iterator_free (iter);
  g_list_free (pushed_pads);
  gst_event_unref (event);

  return res;
}


static void
gst_conditional_selector_class_init (GstConditionalSelectorClass * klass)
{
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);

  /* Add source and sink pads template */
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&main_sink_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&aux_sink_factory));

  klass->check_condition =
      GST_DEBUG_FUNCPTR (gst_conditional_selector_check_condition);

  gst_element_class_set_static_metadata (gstelement_class,
      "Input Conditional Selector",
      "Source/Video",
      "Input multiplexer between two sinks depending on some condition",
      "Veo-Labs http://www.veo-labs.com/");
}

static void
gst_conditional_selector_init (GstConditionalSelector * csel)
{
  GST_INFO_OBJECT (csel, "Conditional selector initialize instance");

  /* Create a new main pad from template */
  csel->main_pad = gst_pad_new_from_static_template (&main_sink_factory,
      "main_sink");
  if (csel->main_pad == NULL) {
    GST_ERROR_OBJECT (csel, "Create main sink pad error");
    return;
  }

  /* Configure the main pad functions */
  gst_pad_set_chain_function (csel->main_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_chain));
  gst_pad_set_event_function (csel->main_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_event));
  gst_pad_set_query_function (csel->main_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_query));

  /* Add main pad to the element */
  gst_element_add_pad (GST_ELEMENT (csel), csel->main_pad);

  /* Create a new main pad from template */
  csel->aux_pad = gst_pad_new_from_static_template (&aux_sink_factory,
      "aux_sink");
  if (csel->aux_pad == NULL) {
    GST_ERROR_OBJECT (csel, "Create aux sink pad error");
    return;
  }

  /* Configure the aux pad functions */
  gst_pad_set_chain_function (csel->aux_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_chain));
  gst_pad_set_event_function (csel->aux_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_event));
  gst_pad_set_query_function (csel->aux_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_sink_query));

  gst_element_add_pad (GST_ELEMENT (csel), csel->aux_pad);

  /* Request src pad from static template */
  csel->src_pad = gst_pad_new_from_static_template (&src_factory, "src");
  if (csel->src_pad == NULL) {
    GST_ERROR_OBJECT (csel, "Create src pad error");
    return;
  }

  /* Configure the src pad functions */
  gst_pad_set_event_function (csel->src_pad,
      GST_DEBUG_FUNCPTR (gst_conditional_selector_src_event));

  gst_element_add_pad (GST_ELEMENT (csel), csel->src_pad);

  /* Default values for flags corrupted */
  csel->condition = CONDITION_BUFFER_CORRUPTED;
  csel->condition_active = FALSE;
  csel->flushing = FALSE;
  csel->eos = FALSE;
  g_mutex_init (&csel->lock);
  g_cond_init (&csel->cond);
}

/* Element registration */
static gboolean
plugin_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (conditional_selector_debug,
      "conditionalselector", 0, "conditionalselector");

  return gst_element_register (plugin, "conditionalselector",
      GST_RANK_NONE, GST_TYPE_CONDITIONAL_SELECTOR);
}

#ifndef PACKAGE
#define PACKAGE "Veo-Labs"
#endif

#ifndef VERSION
#define VERSION "1.0"
#endif

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    conditionalselector,
    "ConditionalSelector",
    plugin_init, VERSION, "LGPL", "Veo-Labs", "http://www.veo-labs.com/")
